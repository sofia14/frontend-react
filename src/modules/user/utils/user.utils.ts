import { authApi } from '../../auth/auth.api'

export function getUserName() {
    const user = authApi.getUser()
    if (user) {
        return `${user.name} ${user.surname}`
    }

    return ''
}
