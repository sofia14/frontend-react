import { apiRequest } from '../core/request/api.request'

export interface User {
    id: string
    name: string
    surname: string
    email: string
    birthday: string
    token: string
}

export interface AuthApi {
    login (email: string, password: string): Promise<User>

    logout (): void

    getUser (): User

    setUser (user: User): void

    isLoggedIn (): boolean
}

const object = {
    async login (email: string, password: string) {
        const user = await apiRequest.post(`http://localhost:4002/api/v1/login`, {
            email,
            password
        })

        this.setUser(user)
    },

    async logout () {
        localStorage.clear()
    },

    getUser () {
        const user = localStorage.getItem('user')
        return user ? JSON.parse(user) : undefined
    },

    setUser (user: User) {
        localStorage.setItem('user', JSON.stringify(user))
    },

    isLoggedIn () {
        return !!this.getUser()
    },
}

export const authApi: AuthApi = Object.create(object)
