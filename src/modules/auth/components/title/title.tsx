import React, { FunctionComponent } from 'react'

import { TitleStyled } from './title.styles'

export interface TitleProps {
    header: string
    label: string
}

export const Title: FunctionComponent<TitleProps> = (
    {
        label,
        header,
    },
) => {
    return (
        <TitleStyled>
            <h1>{header}</h1>
            <span>{label}</span>
        </TitleStyled>
    )
}
