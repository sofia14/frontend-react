import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const TitleStyled = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;

    > h1 {
      margin: 0;
    }

    > span {
      margin: 0.5em 0 2em 0;
      color: ${colors.black};
    }
`
