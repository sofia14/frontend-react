import React, { FunctionComponent } from 'react'

import LogoImage from '../../../../assets/images/logo.svg'
import { LogoWrapper } from './logo.styles'

export interface LogoProps {

}

export const Logo: FunctionComponent<LogoProps> = () => {
    return (
        <LogoWrapper>
            <img src={LogoImage} alt="Logo"/>
        </LogoWrapper>
    )
}
