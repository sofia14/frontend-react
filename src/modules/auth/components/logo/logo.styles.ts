import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const LogoWrapper = styled.div`
     display: flex;
    justify-content: center;

    > img {
      width: 100px;
      padding: 1em;
      border-radius: 1em;
      box-shadow: 0 0 10px ${colors.white};
      margin-bottom: 1.5em;
    }
`
