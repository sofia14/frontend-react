import styled from 'styled-components'

export const SocialButtonsWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;

    > button {
      width: calc(50% - 0.5em);
    }

    > button + button {
      margin-left: 1em;
    }
`
