import React from 'react'

import { SocialButtonsWrapper } from './social-buttons.styles'
import { Button } from '../../../../ui-kit/components/button/button'

export const SocialButtons = () => {
    return (
        <SocialButtonsWrapper>
            <Button
                color="primary"
                label="Facebook"
            />
            <Button
                color="secondary"
                label="Google"
            />
        </SocialButtonsWrapper>
    )
}
