import React, { FunctionComponent } from 'react'

import { ORWrapper } from './or.styles'

export const Or: FunctionComponent = (
    {
        children,
    }
) => {
    return (
        <ORWrapper>
            {children}
        </ORWrapper>
    )
}
