import styled from 'styled-components'
import { colors } from '../../../../assets/styles/colors'

export const NewAccountWrapper = styled.div`
    margin-top: 2em;
    justify-content: center;
    text-align: right;

    > a {
      color: ${colors.secondary}
    }

    > span {
      font-weight: bold;
    }

    > * + * {
      margin-left: 0.5em;
    }
`
