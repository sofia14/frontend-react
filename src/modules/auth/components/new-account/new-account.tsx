import React from 'react'
import { Link } from 'react-router-dom'

import { NewAccountWrapper } from './new-account.styles'

export const NewAccount = () => {
    return (
        <NewAccountWrapper>
            <span>Don't have account?</span>
            <Link to="/sign-up">Sign up</Link>
        </NewAccountWrapper>
    )
}
