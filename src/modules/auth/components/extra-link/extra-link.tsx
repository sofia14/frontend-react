import React, { FunctionComponent } from 'react'
import { Link } from 'react-router-dom'

import { ExtraLinkWrapper } from './extra-link.styles'

export interface ExtraLinkProps {
    label: string
    url: string
}

export const ExtraLink: FunctionComponent<ExtraLinkProps> = (
    {
        label,
        url,
    },
) => {
    return (
        <ExtraLinkWrapper>
            <Link to={url}>{label}</Link>
        </ExtraLinkWrapper>
    )
}
