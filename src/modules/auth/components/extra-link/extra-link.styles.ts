import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const ExtraLinkWrapper = styled.div`
    margin-top: 0.5em;
    display: flex;
    justify-content: flex-end;

    > a {
      text-decoration: none;
      color: ${colors.black};
    }
`
