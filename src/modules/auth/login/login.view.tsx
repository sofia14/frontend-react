import React, { FunctionComponent } from 'react'

import { Form, IValues, required } from '../../core/forms/form'
import { ExtraLink } from '../components/extra-link/extra-link'
import { SocialButtons } from '../components/social-buttons/social-buttons'
import { NewAccount } from '../components/new-account/new-account'
import { Or } from '../components/or/or'
import { Layout } from '../../../ui-kit/layouts/layout'
import { Logo } from '../components/logo/logo'
import { Title } from '../components/title/title'
import { RouteComponentProps } from 'react-router-dom'
import { authApi } from '../auth.api'

export interface LoginViewProps extends RouteComponentProps {

}

export const LoginView: FunctionComponent<LoginViewProps> = (
    {
        history,
    },
) => {

    return (
        <Layout>
            <Logo/>
            <Title
                header="Welcome back!"
                label="Log in to your account"
            />

            <Form
                onSubmit={onLogin}
                submitCallback={submitCallback}
                defaultValues={{ email: 'tomas.svojanovsky11@gmail.com', password: '123456789' }}
                validationRules={{
                    email: [{ validator: required }],
                    password: [{ validator: required }],
                }}
                submitLabel="LOG IN"
            >
                <Form.Field
                    label="Email"
                    name="email"
                    placeholder="Enter email"
                    type="email"
                />

                <Form.Field
                    label="Password"
                    name="password"
                    placeholder="Enter password"
                    type="password"
                />

                <ExtraLink
                    label="Forgot password?"
                    url="/forgot-password"
                />
            </Form>

            <Or>
                Or connect using
            </Or>

            <SocialButtons/>

            <NewAccount/>
        </Layout>
    )

    async function onLogin (values: IValues) {
        let success = false

        try {
            const { email, password } = values
            await authApi.login(email, password)
            success = true
        } catch (e) {
            console.error(e)
        }

        return {
            success,
        }
    }

    function submitCallback () {
        history.push('/tournaments')
    }
}
