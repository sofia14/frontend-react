import React from 'react'

import { Button } from '../../../ui-kit/components/button/button'
import { DownloadSubMenuWrapper } from './download-sub-menu.styles'

export const DownloadSubMenu = () => {
    return (
        <DownloadSubMenuWrapper>
            <Button label="Players"/>
            <Button label="By tournaments"/>
        </DownloadSubMenuWrapper>
    )
}
