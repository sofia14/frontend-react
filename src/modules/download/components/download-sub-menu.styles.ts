import styled from 'styled-components'

export const DownloadSubMenuWrapper = styled.div`
  display: flex;
  margin-top: 0.5em;
  padding: 0 1em;
  
  > button {
    width: 50%;
  
    &:first-child {
      margin-right: 0.5em;
    }
    
    &:last-child {
      margin-left: 0.5em;
    }
  }
`
