import React from 'react'

import { ContentListContainer } from '../../core/styles/container.styles'

export const DownloadPlayersView = () => {
    return (
        <ContentListContainer> Download players
        </ContentListContainer>
    )
}
