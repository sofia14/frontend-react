import React from 'react'
import { Redirect, Switch, useRouteMatch } from 'react-router-dom'

import { PrivateRoute } from '../../../routing/private-route'
import { DownloadPlayersView } from './download-players.view'
import { DownloadViewWrapper } from './download-view.styles'
import { DownloadSubMenu } from '../components/download-sub-menu'
import { DownloadPlayersByTournamentView } from './download-players-by-tournament.view'

export const DownloadView = () => {
    const { path } = useRouteMatch()

    return (
        <DownloadViewWrapper>
            <DownloadSubMenu/>

            <Redirect from={`${path}`} to={`${path}/players`}/>
            <Switch>
                <PrivateRoute path={`${path}/players`} component={DownloadPlayersView}/>
                <PrivateRoute path={`${path}/players-by-tournament`} component={DownloadPlayersByTournamentView}/>
            </Switch>
        </DownloadViewWrapper>
    )
}
