import React, { Component, FormEvent, FunctionComponent } from 'react'
import { Input } from '../../../ui-kit/components/input/input'
import { Button } from '../../../ui-kit/components/button/button'

interface IFormContext {
    errors: IErrors
    values: IValues
    setValue?: (fieldName: string, value: any) => void
    validate?: (fieldName: string, value: any) => void
}

const FormContext = React.createContext<IFormContext>({
    errors: {},
    values: {}
})

export interface ISubmitResult {
    success: boolean
    errors?: IErrors
}

interface IErrors {
    [key: string]: string[]
}

export type Validator = (
    fieldName: string,
    values: IValues,
    args?: any
) => string

export const required: Validator = (
    fieldName: string,
    values: IValues,
    args?: any
): string =>
    values[fieldName] === undefined ||
    values[fieldName] === null ||
    values[fieldName] === ''
        ? 'This must be populated'
        : ''

export const minLength: Validator = (
    fieldName: string,
    values: IValues,
    length: number
): string =>
    values[fieldName] && values[fieldName].length < length
        ? `This must be at least ${length} characters`
        : ''

interface IValidation {
    validator: Validator
    arg?: any
}

interface IValidationProp {
    [key: string]: IValidation | IValidation[]
}

export interface IValues {
    [key: string]: any
}

interface IFieldProps {
    name: string
    label?: string
    type?: 'text' | 'email' | 'select' | 'password'
    options?: string[]
    placeholder: string
}

interface IFormProps {
    defaultValues: IValues
    validationRules: IValidationProp
    onSubmit: (values: IValues) => Promise<ISubmitResult>
    submitCallback: () => void
    submitLabel: string
}

interface IState {
    values: IValues
    errors: IErrors
    submitting: boolean
    submitted: boolean
}

export class Form extends Component<IFormProps, IState> {

    static Field: FunctionComponent<IFieldProps> = props => {
        const { name, placeholder, type, label } = props
        const handleChange = (
            text: string,
            context: IFormContext
        ) => {
            if (context.setValue) {
                context.setValue(props.name, text)
            }
        }

        return (
            <FormContext.Consumer>
                {context => (
                    <div className='form-group'>
                        {(type === 'text' || type === 'email' || type === 'password') && (
                            <Input
                                type={type}
                                label={label}
                                placeholderInner={placeholder}
                                handleChange={e => handleChange(e, context)}
                                value={context.values[name]}
                            />
                        )}

                        {context.errors[name] &&
                        context.errors[name].length > 0 &&
                        context.errors[name].map(error => (
                            <span key={error} className='form-error'>
                                {error}
                            </span>
                        ))}
                    </div>
                )}
            </FormContext.Consumer>
        )
    }

    constructor(props: IFormProps) {
        super(props)

        const errors: IErrors = {}
        Object.keys(props.defaultValues).forEach(fieldName => {
            errors[fieldName] = []
        })

        this.state = {
            errors,
            submitted: false,
            submitting: false,
            values: props.defaultValues
        }
    }

    render() {
        const context = {
            errors: this.state.errors,
            setValue: this.setValue,
            validate: this.validate,
            values: this.state.values
        }

        return (
            <FormContext.Provider value={context}>
                <form
                    className='form'
                    noValidate={true}
                    onSubmit={this.handleSubmit}
                >
                    {this.props.children}

                    <br/>
                    <br/>

                    <Button
                        type="submit"
                        disabled={this.state.submitting || this.state.submitted}
                        label={this.props.submitLabel}
                        color="primary"
                    />
                </form>
            </FormContext.Provider>
        )
    }

    setValue = (fieldName: string, value: any) => {
        const newValues = { ...this.state.values, [fieldName]: value };
        this.setState({ values: newValues });
    };

    validate = (fieldName: string, value: any): string[] => {
        const rules = this.props.validationRules[fieldName]
        const errors = []

        if (Array.isArray(rules)) {
            rules.forEach(rule => {
                const error = rule.validator(fieldName, this.state.values, rule.arg)
                if (error) {
                    errors.push(error)
                }
            })
        } else if (rules) {
            const error = rules.validator(fieldName, this.state.values, rules.arg)
            if (error) {
                errors.push(error)
            }
        }

        const newErrors = { ...this.state.errors, [fieldName]: errors }
        this.setState({ errors: newErrors })

        return errors
    }

    validateForm() {
        const errors: IErrors = {};
        let haveError: boolean = false;

        Object.keys(this.props.defaultValues).forEach(fieldName => {
            errors[fieldName] = this.validate(
                fieldName,
                this.state.values[fieldName]
            )
            if (errors[fieldName].length > 0) {
                haveError = true
            }
        })
        this.setState({ errors })

        return !haveError
    }

    handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        const { onSubmit, submitCallback } = this.props

        if (this.validateForm()) {
            this.setState({ submitting: true })
            const result = await onSubmit(this.state.values)
            this.setState({
                errors: result.errors || {},
                submitted: result.success,
                submitting: false
            })

            if (submitCallback && !result.errors) {
                submitCallback()
            }
        }
    }
}

Form.Field.defaultProps = {
    type: 'text'
}
