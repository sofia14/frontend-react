export const apiRequest = {
    get(url: string) {
        return fetch(url, {
            method: 'GET',
        }).then(res => {
            return res.json()
        })
    },

    post(url: string, body: any) {
        return fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }).then(async (data) => {
            try {
                return await data.json()
            } catch (e) {

            }
        })
    },

    patch(url: string, body: any) {
        return fetch(url, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }).then(async (data) => {
            try {
                return await data.json()
            } catch (e) {

            }
        })

    }


}
