import { useEffect, useReducer, useState } from 'react'

export interface FetchHookProps<T> {
    fetchCallback: (filters: any) => any
    data: T
    filters?: any
}

export interface FetchOneHookProps<T> {
    fetchCallback: () => any
    data: T
}

const dataFetchReducer = (state: any, action: any) => {
    switch (action.type) {
        case 'FETCH_INIT':
            return {
                ...state,
                isLoading: true,
                isError: false
            }
        case 'FETCH_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
            }
        case 'FETCH_FAILURE':
            return {
                ...state,
                isLoading: false,
                isError: true,
            }
        default:
            throw new Error()
    }
}

export function FetchHook<T>(
    {
        fetchCallback,
        data,
        filters = {},
    }: FetchHookProps<T>
) {
    const [_filters, _setFilters] = useState(filters)

    const [state, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
        isError: false,
        data,
    })

    function setFilters (filters: {}) {
        _setFilters({
            ..._filters,
            ...filters,
        })
    }

    useEffect(() => {
        (async () => {
            dispatch({ type: 'FETCH_INIT' })

            try {
                const result = await fetchCallback(_filters)

                dispatch({ type: 'FETCH_SUCCESS', payload: result })
            } catch (error) {
                dispatch({ type: 'FETCH_FAILURE' })
            }
        })()
    }, [_filters])

    return [state, setFilters]
}

export function FetchOneHook<T>(
    {
        fetchCallback,
        data
    }: FetchOneHookProps<T>
) {
    const [state, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
        isError: false,
        data
    })

    useEffect(() => {
        (async () => {
            dispatch({ type: 'FETCH_INIT' })

            try {
                const result = await fetchCallback()

                dispatch({ type: 'FETCH_SUCCESS', payload: result })
            } catch (error) {
                dispatch({ type: 'FETCH_FAILURE' })
            }
        })()
    }, [])

    return [state]
}
