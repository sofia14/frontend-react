import styled from 'styled-components'

export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1.5em 1em 1em 1em;
`

export const ContentListContainer = styled(ContentContainer)`
    > div + div {
      margin-top:  1.5em;
    }
`

export const TopBar = styled.div`
  padding: 0 2em 0 2em;
  position: relative;
  
  > .icons {
    display: flex;
    justify-content: flex-end;
  }
`
