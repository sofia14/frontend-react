import { IApplicationState } from '../../../../store'

export const uiSelectors = {
    selectTitle(state: IApplicationState) {
        return state.ui.title
    }
}
