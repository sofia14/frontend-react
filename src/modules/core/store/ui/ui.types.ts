export enum UIActionTypes {
    SET_TITLE = 'UI/SET_TITLE',
}

export interface IUISetTitleAction {
    type: UIActionTypes.SET_TITLE
    payload: {
        title: string,
    },
}

export type UIActions =
    | IUISetTitleAction
