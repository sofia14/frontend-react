import { UIActions, UIActionTypes } from './ui.types'

export interface UIState {
    title: string
}

const initialState: UIState = {
    title: ''
}

export function UIReducer(state = initialState, action: UIActions): UIState {
    switch (action.type) {
        case UIActionTypes.SET_TITLE:
            return { ...state, title: action.payload.title }

        default:
            return state
    }
}
