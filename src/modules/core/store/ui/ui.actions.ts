import { UIActionTypes } from './ui.types'

export function setTitleAction(title: string) {
    return {
        type: UIActionTypes.SET_TITLE,
        payload: {
            title
        }
    }
}
