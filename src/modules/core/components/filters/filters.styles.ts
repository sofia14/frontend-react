import styled from 'styled-components'

export const FiltersWrapper = styled.div`
  position: relative;
  display: flex;
  
  > .content {
      flex: 1;
      
        > div {
            display: none;
            height: 64px;
            
            &.active {
              display: flex;
            }
      }
  }
 
  
  > .arrows {
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-left: 0.5em;
    
     > span {
      &.disabled {
          opacity: 0.6;
          pointer-events: none;
      }
     }
  }
`

