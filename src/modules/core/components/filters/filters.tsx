import React, { FunctionComponent, useState } from 'react'
import classnames from 'classnames'

import { Search } from '../../../../ui-kit/components/search/search'
import { FiltersWrapper } from './filters.styles'
import { Select, SelectItem } from '../../../../ui-kit/components/select/select'

export interface FiltersProps {
    handleSearch(value: string): void

    handleSelectYear(item: SelectItem): void

    loading: boolean
}

export const Filters: FunctionComponent<FiltersProps> = (
    {
        loading,
        handleSearch,
        handleSelectYear
    }
) => {
    const years: SelectItem[] = [
        {
            value: 2020,
            label: '2020'
        },
        {
            value: 2019,
            label: '2019'
        }
    ]

    const [selectYear, setSelectYear] = useState(years[0])

    const [index, setIndex] = useState(0)

    const onSelect = (item: SelectItem) => {
        setSelectYear(item)
        handleSelectYear(item)
    }

    const onSetIndex = (move: number) => {
        setIndex(index + move)
    }

    return (
        <FiltersWrapper>
            <div className="content">
                <div className={classnames({ 'active': (index == 0) })}>
                    <Select
                        handleSelected={onSelect}
                        options={years}
                        selected={selectYear}
                        placeholder="Please select..."
                    />
                </div>

                <div className={classnames({ 'active': (index == 1) })}>
                    <Search
                        handleSearch={handleSearch}
                        loading={loading}
                    />
                </div>
            </div>

            <div className="arrows">
                <span
                    onClick={() => onSetIndex(-1)}
                    className={classnames('material-icons', { 'disabled': (index == 0) })}>
                    expand_less
                </span>

                <span
                    onClick={() => onSetIndex(1)}
                    className={classnames('material-icons', { 'disabled': (index == 1) })}>
                    expand_more
                </span>
            </div>
        </FiltersWrapper>
    )
}
