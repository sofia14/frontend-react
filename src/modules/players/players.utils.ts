import { PlayerDetail } from './player-detail/player-detail.api';

export function getPlayerName(player: PlayerDetail) {
    if (!player.name || !player.name) {
        return '-'
    }

    return `${player.name} ${player.surname}`
}
