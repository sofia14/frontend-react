import React, { FunctionComponent } from 'react'

import { Card, CardImage, CardMatchView, PlayerCardWrapper } from './player-card.styles'
import { ListPlayer } from '../../player-list/player-list.api'
import { getAge, getRankStatusIcon } from './player-card.utils'
import { Image } from '../../../../ui-kit/components/image/image'

export interface PlayerCardProps {
    player: ListPlayer
    handleClick: () => void
}

export const PlayerCard: FunctionComponent<PlayerCardProps> = (
    {
        player,
        handleClick
    }
) => {
    return (
        <PlayerCardWrapper onClick={handleClick}>
            <Card>
                <CardImage>
                    <Image image={player.image}/>
                </CardImage>

                <div className="clash-card__level clash-card__level--giant">
                    <div>Rank:</div>
                    <div className="rank">
                        <div>{player.currentRank}</div>
                        <span className="material-icons rank-status">
                            {getRankStatusIcon(player)}
                        </span>
                    </div>
                </div>
                <div className="clash-card__unit-name">{`${player.name} ${player.surname}`}</div>
                <div className="clash-card__unit-description">
                    <CardMatchView>
                        <div className="tournament">Shenzen open</div>
                        <div className="players">
                            <div>Karolina Muchova</div>
                            <div>-:-</div>
                            <div>Elina Svitolina</div>
                        </div>
                    </CardMatchView>

                    <CardMatchView>
                        <div className="tournament">Shenzen open</div>
                        <div className="players">
                            <div className="winner">Aryna sabalenka</div>
                            <div>2:0</div>
                            <div className="loser">Elina Svitolina</div>
                        </div>
                    </CardMatchView>

                    <CardMatchView>
                        <div className="tournament">Shenzen open</div>
                        <div className="players">
                            <div className="winner">Aryna sabalenka</div>
                            <div>2:0</div>
                            <div className="loser">Elina Svitolina</div>
                        </div>
                    </CardMatchView>
                </div>

                <div className="clash-card__unit-stats clash-card__unit-stats--giant clearfix">
                    <div className="one-third">
                        <div className="stat">{getAge(player.birthday)}</div>
                        <div className="stat-value">Age</div>
                    </div>

                    <div className="one-third">
                        <div className="stat">Hard</div>
                        <div className="stat-value">Surface</div>
                    </div>

                    <div className="one-third no-border">
                        <div className="stat">80%</div>
                        <div className="stat-value">Wins</div>
                    </div>

                </div>
            </Card>
        </PlayerCardWrapper>
    )
}
