import { getAge } from './player-card.utils';

describe('Player card template', () => {

    test('age util', () => {
        const age = getAge('2015-07-25T00:00:00Z', new Date('2019-07-25T00:00:00Z'))

        expect(age).toBe(4)
    })
})
