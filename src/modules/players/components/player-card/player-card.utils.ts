import moment from 'moment'

import { ListPlayer } from '../../player-list/player-list.api';

export function getAge(date: string, now = new Date()) {
    if (!date) {
        return ''
    }

    const age = moment(date).diff(now.toISOString(), 'years')

    return Math.abs(age)
}

export function getRankStatusIcon(player: ListPlayer) {
    const diff = player.currentRank - player.previousRank

    if (diff > 0) {
        return 'arrow_upward'
    } else if (diff < 0) {
        return 'arrow_downward'
    } else {
        return 'arrow_forward'
    }
}
