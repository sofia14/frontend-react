import styled from 'styled-components'
import { colors } from '../../../../assets/styles/colors'

export const PlayerCardWrapper = styled.div`
    display: flex;
    line-height: 20px;
    font-size: 14px;
    cursor: pointer;
    
    .clash-card__level {
      text-transform: uppercase;
      font-size: 1em;
      margin-bottom: 3px;
      display: flex;
      justify-content: center;
      
      > .rank {
        display: flex;
        align-items: flex-start;
        
        > span {
          font-size: 1em;
          margin-left: 0.5em;
          margin-top: 0.1em;
        }
      }
    }
    
    .clash-card__unit-name {
      font-size: 26px;
      color: black;
      font-weight: 900;
      margin-bottom: 5px;
      padding: 0 20px;
    }
    
    .clash-card__unit-description {
      padding: 20px;
      height: 160px;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    
    .clash-card__unit-stats--giant {
      background: ${colors.secondary};
    }
    
    .clash-card__unit-stats--giant .one-third {
      border-right: 1px solid #f18970;
    }
    
    .clash-card__unit-stats {
      color: white;
      font-weight: 700;
      border-bottom-left-radius: 14px;
      border-bottom-right-radius: 14px;
      display: flex;
    }
    
    .clash-card__unit-stats .one-third {
      width: 33%;
      padding: 1.2em 1em 0.5em 1.2em;
    }
    
    .clash-card__unit-stats sup {
      position: absolute;
      bottom: 4px;
      font-size: 45%;
      margin-left: 2px;
    }
    
    .clash-card__unit-stats .stat {
      position: relative;
      font-size: 24px;
      margin-bottom: 10px;
    }
    
    .clash-card__unit-stats .stat-value {
      text-transform: uppercase;
      font-weight: 400;
      font-size: 12px;
    }
    
    .clash-card__unit-stats .no-border {
      border-right: none;
    }
    
    .clearfix:after {
      visibility: hidden;
      display: block;
      font-size: 0;
      content: " ";
      clear: both;
      height: 0;
    }
    
    .slick-prev {
      left: 100px;
      z-index: 999;
    }
`

export const Card = styled.div`
    width: 300px;
    display: inline-block;
    margin: auto;
    border-radius: 19px;
    position: relative;
    text-align: center;
    box-shadow: 0 5px 20px -10px #111111;
    z-index: 1;
`

export const CardImage = styled.div`
    position: relative;
    height: 230px;
    margin-bottom: 1em;
    border-top-left-radius: 14px;
    border-top-right-radius: 14px;
    display: flex;
    justify-content: center;
    background: ${colors.primary};
    
    > img {
        height: 100%;
        position: absolute;
    }
`

export const CardMatchView = styled.div`
  > .tournament {
    color: ${colors.primary}
  }

  > .players {
    display: flex;
    justify-content: space-between;
    
    > .winner {
      color: ${colors.green}
    }
    
    > .loser {
      color ${colors.red}
    }
    
    > div {
      white-space: nowrap;
      overflow: hidden; 
      text-overflow: ellipsis;
    
      &:first-child {
        width: 40%;
        text-align: left;
      }
      
      &:nth-child(2) {
        width: 10%;
      }
      
      &:last-child {
        width: 40%;
        text-align: right;
      }
    }
  }
`
