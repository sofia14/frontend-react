import React, { FunctionComponent } from 'react'

import { getPlayerName } from '../../players.utils'
import { PlayerDetail } from '../../player-detail/player-detail.api'
import { PlayerDetailTopBarWrapper } from '../../player-detail/player-detail.styles'

export interface PlayerDetailTopBarProps {
    player: PlayerDetail
    handleClick: () => void
}

export const PlayerDetailTopBar: FunctionComponent<PlayerDetailTopBarProps> = (
    {
        player,
        handleClick,
    },
) => {

    return (
        <PlayerDetailTopBarWrapper>
           <span
               onClick={handleClick}
               className="material-icons">
                arrow_back
            </span>

            <h1 className="name">{getPlayerName(player)}</h1>
            <div className="info">{player.country || '-'}</div>

            <div className="img">
                <img src={player.image} alt="Player photo"/>
            </div>
        </PlayerDetailTopBarWrapper>
    )
}
