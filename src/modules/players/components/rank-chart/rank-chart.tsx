import React, { useEffect, useRef, useState } from 'react'
import Chart from 'chart.js'

import { RankChartWrapper } from './rank-chart.styles'
import { colors } from '../../../../assets/styles/colors'
import { ChartPeriod } from './chart-period'

const chartConfig = {
    type: 'bar',
    data: {
        labels: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        datasets: [{
            data: [120, 121, 122, 130, 68, 57, 47, 33, 10, 15, 17, 18],
            backgroundColor: colors.secondary
        }]
    },
    options: {
        legend: {
            display: false
        }
    }
}

export const RankChart = () => {

    const chartContainer = useRef(null)
    const [, setChartInstance] = useState(null)

    useEffect(() => {
        if (chartContainer && chartContainer.current) {
            const instance = new Chart(chartContainer.current as any, chartConfig)
            setChartInstance(instance as any)
        }
    }, [chartContainer])

    return (
        <RankChartWrapper>
            <div className="title">Ranking</div>

            <ChartPeriod/>

            <canvas ref={chartContainer}/>
        </RankChartWrapper>
    )
}
