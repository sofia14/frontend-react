import styled from 'styled-components'

export const RankChartWrapper = styled.div`
  display: flex;
  flex-direction: column;

  > .title {
    font-size: 1.2em;
  } 
`
