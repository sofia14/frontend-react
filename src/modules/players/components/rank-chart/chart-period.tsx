import React, { useState } from 'react'
import classNames from 'classnames'

import { ChartPeriodWrapper } from './chart-period.styles'

export const ChartPeriod = () => {
    const period = ['Year', 'Month', 'Week', 'Day']

    const [active, setActive] = useState(period[0])

    const onClick = (item: string) => {
        setActive(item)
    }

    return (
        <ChartPeriodWrapper>
            {period.map(item => (
                <div
                    key={item}
                    className={classNames({ 'active': item === active })}
                    onClick={() => onClick(item)}
                >{item}</div>
            ))}
        </ChartPeriodWrapper>
    )
}
