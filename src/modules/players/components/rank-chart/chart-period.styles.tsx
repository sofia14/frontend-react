import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const ChartPeriodWrapper = styled.div`
    display: flex;
    color: ${colors.white};
    justify-content: space-between;
    margin-bottom: 0.5em;
    
    > div {
      background-color: ${colors.primary};
      box-shadow: 0 0 5px #8da7c9;
      padding: 0.2em 1em;
      
      &.active {
        background-color: ${colors.secondary};
          box-shadow: 0 0 5px #f5a693;
      }
    }
`
