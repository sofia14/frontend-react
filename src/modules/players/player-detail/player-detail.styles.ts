import styled from 'styled-components'

import { colors } from '../../../assets/styles/colors'
import { SectionTopWrapper } from '../../../ui-kit/components/section-top-bar/section-top-bar.styles'

export const Section = styled.div`
  margin-top: 5em;
  padding: 0 1em;
  
  > div:nth-child(2) {
    display: flex;
    
    > div {
      &:last-child {
        padding: 0 1em;
        flex: 1;
      }
    }
  }
  
  .title {
    font-weight: bold;
    margin-bottom: 0.5em;
    font-size: 1.2em;
  }
  
  .square {
    width: 7.5em;
    height: 7.5em;
    background: linear-gradient(to left, #597fb0, #3d5a80);
    box-shadow: 0 0 5px #8da7c9;
    border-radius: 10px;
    padding: 1em;
    color: ${colors.white};
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: flex-end;
    
    > .count {
      font-weight: bold;
      font-size: 2em;
    }
  }
`

export const LineGroup = styled.div`
  width: 100%;

  > .info {
    display: flex;
    justify-content: space-between;
  } 
`

export const Line = styled.div`
    background-color: #f1f1f1;
    height: 5px;
    margin-top: 0.5em;
    
    > div {
      height: 5px;
    }
`

export const GreenLine = styled(Line)`
  > div {
     background-color: green;
  }
`

export const RedLine = styled(Line)`
  > div {
    background-color: #ee4035;
  }
`

export const GreyLine = styled(Line)`
  > div {
    background-color: #99aab5;
  }
`

export const QuickStats = styled.div`
  > div + div {
    margin-top: 0.8em;
  }
  
  > div {
    &:last-child {
      margin-top: 1em;
    }
  }
`

export const HorizontalLine = styled.hr`
  background-color: #f1f1f1;
  margin-top: 1em;
`

export const Ranking = styled.div`
  display: flex;
  align-items: center;
  margin-top: 1em;

  > div {
    &:first-child {
      font-size: 1.6em;
      font-weight: bold;
    }
    
    &:last-child {
      color: #99aab5;
      margin-left: 0.5em;
    }
  }
`

export const Percentage = styled.div`
    display: flex;
    align-items: center;

    > div {
        &:first-child {
            font-weight: bold;
            color: ${colors.primary};
            font-size: 1.6em;
        }
    
        &:last-child {
            color: #99aab5;
            margin-left: 0.5em;
        }
    }
`

export const PlayerDetailTopBarWrapper = styled(SectionTopWrapper)`
  > .name {
    margin-bottom: 0.2em;
  }
  
  > .info {
     padding-bottom: 3em;
  }
  
  > .img {
    border-radius: 50%;
    background-color: #fff;
    position: absolute;
    bottom: -50px;
    box-shadow: 0 0 2px ${colors.black};
    height: 100px;
    
    > img {
      max-height: 100px;
      border-radius: 50%;
    } 
  }
`
