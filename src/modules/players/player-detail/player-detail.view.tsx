import React, { FunctionComponent, useEffect } from 'react'
import { RouteComponentProps, useParams } from 'react-router-dom'
import { connect } from 'react-redux'

import { FetchOneHook } from '../../core/request/hooks.request'
import { fetchPlayer, PlayerDetail } from './player-detail.api'
import {
    GreenLine,
    GreyLine,
    HorizontalLine,
    LineGroup,
    Percentage,
    QuickStats,
    Ranking,
    RedLine,
    Section
} from './player-detail.styles'
import { PlayerDetailTopBar } from '../components/player-detail-top-bar/player-detail-top-bar'
import { setTitleAction } from '../../core/store/ui/ui.actions'
import { RankChart } from '../components/rank-chart/rank-chart'
import { Miscellaneous } from './components/miscellaneous/Miscellaneous'

export interface PlayerDetailProps extends RouteComponentProps {
    setTitle: typeof setTitleAction
}

const Component: FunctionComponent<PlayerDetailProps> = (
    {
        history,
        setTitle
    }
) => {

    const { id } = useParams()
    const [{ data }] = FetchOneHook<PlayerDetail>({
        fetchCallback: () => fetchPlayer(id),
        data: {}
    })

    useEffect(() => {
        setTitle('Player detail')
    }, [])

    const onBack = () => {
        history.push('/players')
    }

    return (
        <div>
            <PlayerDetailTopBar
                player={data}
                handleClick={onBack}
            />

            <Section>
                <div className="title">Games</div>

                <div>
                    <div>
                        <div className="square">
                            <div>Total games played</div>
                            <div className="count">38</div>
                        </div>

                        <Ranking>
                            <div>{data.rank}</div>
                            <div>Ranking</div>
                        </Ranking>
                    </div>

                    <QuickStats>
                        <LineGroup>
                            <div className="info">
                                <div>Win</div>
                                <div>30</div>
                            </div>

                            <GreenLine>
                                <div style={{ 'width': '75%' }}/>
                            </GreenLine>
                        </LineGroup>

                        <LineGroup>
                            <div className="info">
                                <div>Lose</div>
                                <div>5</div>
                            </div>

                            <RedLine>
                                <div style={{ 'width': '15%' }}/>
                            </RedLine>
                        </LineGroup>

                        <LineGroup>
                            <div className="info">
                                <div>Retired</div>
                                <div>3</div>
                            </div>

                            <GreyLine>
                                <div style={{ 'width': '10%' }}/>
                            </GreyLine>
                        </LineGroup>

                        <Percentage>
                            <div>80%</div>
                            <div>Game win</div>
                        </Percentage>
                    </QuickStats>
                </div>

                <HorizontalLine/>

                <Miscellaneous/>

                <HorizontalLine/>

                <RankChart/>

                <div>Last 10 matches</div>

                <div>Last 2 tournaments</div>
            </Section>
        </div>
    )
}

const mapStateToProps = () => {
    return {}
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        setTitle: (title: string) => dispatch(setTitleAction(title))
    }
}

export const PlayerDetailView = connect(
    mapStateToProps,
    mapDispatchToProps
)(Component)
