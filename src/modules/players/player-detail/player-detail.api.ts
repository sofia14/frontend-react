import { apiRequest } from '../../core/request/api.request';

interface Player {
    id: string
    name: string
    surname: string
    image: string
    country: string
    birthday: string
    rank: number
}

export type PlayerDetail = Partial<Player>

export async function fetchPlayer(id: string): Promise<PlayerDetail> {
    return await apiRequest.get(`http://localhost:4002/api/v1/player/${id}`)
}
