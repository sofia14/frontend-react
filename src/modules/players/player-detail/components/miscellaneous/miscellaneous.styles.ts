import styled from 'styled-components'

export const MiscellaneousWrapper = styled.div`
  font-size: 1.2em;
  display: flex;
  flex-direction: column;
`
