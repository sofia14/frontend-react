import React from 'react'

import { MiscellaneousWrapper } from './miscellaneous.styles'
import { TournamentMatch } from '../../../../tournaments/tournament-detail/components/tournament-match'

export const Miscellaneous = () => {
    return (
        <MiscellaneousWrapper>
            <div className="title">Miscellaneous</div>

            <div>Best win</div>

            <TournamentMatch
                players={[
                    {
                        rank: 1,
                        name: 'Ashleigh',
                        surname: 'Barty',
                        result: 2
                    },
                    {
                        rank: 2,
                        name: 'Elina',
                        surname: 'Svitlina',
                        result: 0
                    }
                ]}
            />

            <div>Worst lose</div>

            <TournamentMatch
                players={[
                    {
                        rank: 1,
                        name: 'Ashleigh',
                        surname: 'Barty',
                        result: 0
                    },
                    {
                        rank: 48,
                        name: 'Jil',
                        surname: 'Teichmann',
                        result: 2
                    }
                ]}
            />
        </MiscellaneousWrapper>
    )
}
