import React, { FunctionComponent, useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { connect } from 'react-redux'

import { FetchHook } from '../../core/request/hooks.request'
import { ListPlayer, playerApiList, PlayerListResponse } from './player-list.api'
import { PlayerCard } from '../components/player-card/player-card'
import { setTitleAction } from '../../core/store/ui/ui.actions'
import { Search } from '../../../ui-kit/components/search/search'
import { ContentListContainer, TopBar } from '../../core/styles/container.styles'
import { Pagination } from '../../../ui-kit/components/pagination/pagination'

export interface PlayerListProps extends RouteComponentProps {
    setTitle: typeof setTitleAction
}

const Component: FunctionComponent<PlayerListProps> = (
    {
        history,
        setTitle
    }
) => {
    const [{ data, isLoading }, setFilters] = FetchHook<PlayerListResponse>({
        fetchCallback: playerApiList,
        data: { players: [], count: 0 },
        filters: { offset: 0, limit: 25 }
    })

    const [page, setPage] = useState(0)
    const [search, setSearch] = useState('')

    useEffect(() => {
        setTitle('Player list')
    }, [])

    useEffect(() => {
        setFilters({ offset: page * 25, limit: 25, search })
    }, [page, search])

    const onClick = (id: string) => {
        history.push(`/player/${id}`)
    }

    const onPage = (page: number) => {
        setPage(page)
    }

    const onSearch = (value: string) => {
        setSearch(value)
    }

    return (
        <div>
            <TopBar>
                <Search
                    handleSearch={onSearch}
                    loading={isLoading}
                />
            </TopBar>

            <ContentListContainer>
                {data.players.map((player: ListPlayer) => (
                    <PlayerCard
                        player={player}
                        handleClick={() => onClick(player.id)}
                        key={player.id}
                    />
                ))}

                <Pagination
                    count={data.count}
                    handleClick={onPage}
                    page={page}
                />
            </ContentListContainer>
        </div>
    )
}

const mapStateToProps = () => {
    return {}
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        setTitle: (title: string) => dispatch(setTitleAction(title))
    }
}

export const PlayerListView = connect(
    mapStateToProps,
    mapDispatchToProps
)(Component)
