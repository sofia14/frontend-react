import { Filters, toQueryString } from '../../../utils/request.utils'
import { apiRequest } from '../../core/request/api.request'

export interface ListPlayer {
    id: string
    bio: string
    name: string
    surname: string
    age: number
    birthday: string
    image: string
    currentRank: number
    previousRank: number
    rankStatus: string
}

export interface PlayerListResponse {
    players: ListPlayer[]
    count: number
}

export interface PlayerListParams extends Filters {
    limit: number
    offset: number
}

export async function playerApiList(params: PlayerListParams): Promise<PlayerListResponse> {
    const query = toQueryString(params)
    const response = await apiRequest.get(`http://localhost:4002/api/v1/players${query}`)

    return {
        players: response.data,
        count: response.count,
    }
}
