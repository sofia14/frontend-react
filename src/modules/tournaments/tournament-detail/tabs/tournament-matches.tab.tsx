import React, { FunctionComponent } from 'react'
import classnames from 'classnames'

import { MatchesContent } from '../components/tournament-detail-tabs.styles'
import { TournamentMatch } from '../components/tournament-match'

export interface TournamentMatchesTabProps {
    index: number
    tournament: string
}

export const TournamentMatchesTab: FunctionComponent<TournamentMatchesTabProps> = (
    {
        index,
        tournament,
    },
) => {
    return (
        <MatchesContent className={classnames({ 'active': (index == 1) })}>
            <TournamentMatch
                players={[
                    {
                        rank: 48,
                        name: 'Jil',
                        surname: 'Teichmann',
                        result: 2
                    },
                    {
                        rank: 18,
                        name: 'Petra',
                        surname: 'Martic',
                        result: 0
                    }
                ]}
            />
        </MatchesContent>
    )
}
