import React, { FunctionComponent, useEffect, useState } from 'react'
import classnames from 'classnames'

import { TournamentPlayer } from '../components/tournament-player'
import { PaginationSimple } from '../../../../ui-kit/components/pagination/pagination-simple'
import { PlayersContent } from '../components/tournament-detail-tabs.styles'
import { FetchHook } from '../../../core/request/hooks.request'
import { PlayerListResponse } from '../../../players/player-list/player-list.api'
import { fetchTournamentPlayers } from '../tournament-detail.api'
import { Loader } from '../../../../ui-kit/components/loader/loader'

export interface TournamentPlayersTabProps {
    index: number
    tournament: string
}

export const TournamentPlayersTab: FunctionComponent<TournamentPlayersTabProps> = (
    {
        index,
        tournament
    },
) => {
    const [page, setPage] = useState(0)
    const [pagination, setPagination] = useState({
        offset: 0,
        limit: 5,
    })

    useEffect(() => {
        setFilters(pagination)
    }, [pagination])

    const [{ data, isLoading }, setFilters] = FetchHook<PlayerListResponse>({
        fetchCallback: fetchTournamentPlayers,
        data: { players: [], count: 0 },
        filters: { offset: 0, limit: 5, tournament, type: 'ed5b6223-5ba1-4d78-848a-a0539e5a599d' }
    })

    function onPageChange (pageChange: number) {
        setPage(page + pageChange)
        const offset = page * 5

        setPagination({
            ...pagination,
            offset,
        })
    }

    return (
        <PlayersContent className={classnames({ 'active': (index == 0) })}>
            {isLoading ? <Loader/> : data.players.map((player: any) => (
                <TournamentPlayer
                    id={player.id}
                    image={player.image}
                    rank={player.currentRank}
                    name={player.name}
                    surname={player.surname}
                    country={player.country}
                />
            ))}

            <PaginationSimple
                count={data.count}
                perPage={5}
                page={page}
                handlePageChange={onPageChange}
            />
        </PlayersContent>
    )
}
