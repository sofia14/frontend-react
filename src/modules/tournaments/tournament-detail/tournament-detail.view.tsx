import React, { FunctionComponent, useEffect } from 'react'
import { RouteComponentProps, useParams } from 'react-router-dom'
import { connect } from 'react-redux'

import { ContentContainer } from '../../core/styles/container.styles'
import { setTitleAction } from '../../core/store/ui/ui.actions'
import { FetchOneHook } from '../../core/request/hooks.request'
import { fetchTournament, ITournamentDetail } from './tournament-detail.api'
import { QuickInfo, TopBar } from './tournament-detail.styles'
import { getDate } from '../../../utils/date.utils'
import { getCountry } from '../tournament.utils'
import { ucFirst } from '../../../utils/common.utils'
import { TournamentDetailTabs } from './components/tournament-detail-tabs'
import { formatMoney } from '../../../utils/money.utils'
import { TournamentDetailTicketsChild } from './children/tournament-detail-tickets.child'
import { TournamentWinner } from './components/tournament-winner'

export interface TournamentDetailProps extends RouteComponentProps {
    setTitle: typeof setTitleAction
}

const Component: FunctionComponent<TournamentDetailProps> = (
    {
        history,
        setTitle
    }
) => {

    const { id } = useParams()
    const [{ data: tournament }] = FetchOneHook<ITournamentDetail>({
        fetchCallback: () => fetchTournament(id),
        data: {}
    })

    useEffect(() => {
        setTitle('Tournament detail')
    }, [])

    const onBack = () => {
        history.push('/tournaments')
    }

    return (
        <ContentContainer
        >
            <TopBar>
                 <span
                     onClick={onBack}
                     className="material-icons">
                    arrow_back
                </span>

                <div className="title">{tournament.name}</div>

                <span className="material-icons">
                    system_update_alt
                </span>
            </TopBar>

            <QuickInfo>
                <div className="left">
                    <ul>
                        <li>
                            <div>Start:</div>
                            <div>{getDate(tournament.start)}</div>
                        </li>

                        <li>
                            <div>End:</div>
                            <div>{getDate(tournament.end)}</div>
                        </li>

                        <li>
                            <div>Players:</div>
                            <div>0</div>
                        </li>

                        <li>
                            <div>Money:</div>
                            <div>{formatMoney(tournament.money)}</div>
                        </li>

                        <li>
                            <div>Country:</div>
                            <div>{getCountry(tournament.country)}</div>
                        </li>

                        <li>
                            <div>Surface:</div>
                            <div>{ucFirst(tournament.surface)}</div>
                        </li>
                    </ul>
                </div>

                <TournamentWinner
                    winner={{}}
                />
            </QuickInfo>

            <TournamentDetailTicketsChild/>

            <TournamentDetailTabs
                tournament={id}
            />
        </ContentContainer>
    )
}

const mapStateToProps = () => {
    return {}
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        setTitle: (title: string) => dispatch(setTitleAction(title))
    }
}

export const TournamentDetailView = connect(
    mapStateToProps,
    mapDispatchToProps
)(Component)
