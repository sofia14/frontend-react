import styled from 'styled-components'
import { colors } from '../../../assets/styles/colors'

export const TopBar = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
  
    > .title {
        font-size: 1.4em;
        font-weight: bold;
        margin: 0 auto;
    }
`

export const QuickInfo = styled.div`
  padding: 1em;
  width: 100%;
  background-color: ${colors.primary};
  color: ${colors.white};
  margin-top: 1em;
  display: flex;
  
  > div {
    flex: 1;
  }
  
  > .left {
    > ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        height: 100%;
        
        > li {
          display: flex;
          
          > div {
            &:first-child {
              font-weight: bold;
              min-width: 4.5em;
            }
          }
        }
      }
  }
`
