import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const TournamentDetailTicketsChildWrapper = styled.div`
  width: 100%;
  margin-top: 1em;

  > .title {
    background-color: ${colors.secondary};
    color: ${colors.white};
    padding: 1em;
    text-align: center;
    position: relative;
    
    > .add {
      position: absolute;
      right: 0;
      top: 0;
      transform: translateY(50%);
      padding-right: 0.5em;
    }
  }
  
  > .empty {
    margin-top: 0.5em;
    text-align: center;
  }
`
