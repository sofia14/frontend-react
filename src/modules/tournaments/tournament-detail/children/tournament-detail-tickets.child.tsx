import React, { useState } from 'react'

import { TournamentDetailTicketsChildWrapper } from './tournament-detail-tickets.styles'

export const TournamentDetailTicketsChild = () => {

    const [ticket] = useState([])

    return (
        <TournamentDetailTicketsChildWrapper>
            <div className="title">
                <div>Tickets</div>

                <span className="material-icons add">
                    add
                </span>
            </div>

            {(ticket.length === 0) ? <div className="empty">
                You don't have any tickets for this tournament
            </div> : null}
        </TournamentDetailTicketsChildWrapper>
    )
}
