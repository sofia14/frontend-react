import { apiRequest } from '../../core/request/api.request'
import { toQueryString } from '../../../utils/request.utils'

export interface Tournament {
    name: string
}

export type ITournamentDetail = Partial<Tournament>

export interface TournamentPlayersRequest {
    limit: number
    offset: number
    tournament: string
    type: string
}

export async function fetchTournament (id: string) {
    return apiRequest.get(`http://localhost:4002/api/v1/tournament/${id}`)
}

export async function fetchTournamentPlayers (params: TournamentPlayersRequest) {
    const { tournament, limit, offset, type } = params
    const query = toQueryString({
        limit,
        offset,
        type,
    })

    const response = await apiRequest.get(`http://localhost:4002/api/v1/tournament/${tournament}/players${query}`)

    return {
        players: response.data || [],
        count: response.count || 0
    }
}
