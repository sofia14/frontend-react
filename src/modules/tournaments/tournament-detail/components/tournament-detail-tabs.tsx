import React, { FunctionComponent, useState } from 'react'
import classnames from 'classnames'

import { Tabs } from './tournament-detail-tabs.styles'
import { TournamentPlayersTab } from '../tabs/tournament-players.tab'
import { TournamentMatchesTab } from '../tabs/tournament-matches.tab'

export interface TournamentPlayerProps {
    tournament: string
}

export const TournamentDetailTabs: FunctionComponent<TournamentPlayerProps> = (
    {
        tournament,
    },
) => {

    const [index, setIndex] = useState(0)

    return (
        <Tabs>
            <div className="headers">
                <div
                    onClick={() => setIndex(0)}
                    className={classnames({ 'active': (index == 0) })}
                >
                    <div className="label">Players</div>
                    <span className="material-icons icon">
                        sort
                    </span>
                </div>
                <div
                    onClick={() => setIndex(1)}
                    className={classnames({ 'active': (index == 1) })}
                >
                    <div className="label">Matches</div>
                    <span className="material-icons icon">
                        sort
                    </span>
                </div>
            </div>

            <TournamentPlayersTab
                index={index}
                tournament={tournament}
            />

            <TournamentMatchesTab
                index={index}
                tournament={tournament}
            />
        </Tabs>
    )
}
