import styled from 'styled-components'

export const TournamentWinnerWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
  
    > .name {
      margin-top: 0.5em;
    }
  
    > .img-wrapper {
      margin-top: 0.5em;
      height: 100px;
    }
  
    > div {
        > img {
          height: 100%;
        } 
    }
`

