import React, { FunctionComponent } from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'

import { TournamentPlayerWrapper } from './tournament-player.styles'

export interface TournamentPlayerProps extends RouteComponentProps {
    image: string
    name: string
    surname: string
    rank: number
    country: string
    id: string
}

const Component: FunctionComponent<TournamentPlayerProps> = (
    {
        image,
        name,
        surname,
        rank,
        country,
        id,
        history,
    }
) => {

    const onPlayerDetail = (id: string) => {
        history.push(`/player/${id}`)
    }

    return (
        <TournamentPlayerWrapper
            onClick={() => onPlayerDetail(id)}
        >
            <div className="image">
                <img src={image || `/images/placeholder.jpg`}/>
            </div>

            <div className="player-info">
                <div>{name} {surname}</div>
                <div>{country}</div>
            </div>

            <div className="rank">
                <div>{rank}</div>
            </div>
        </TournamentPlayerWrapper>
    )
}

export const TournamentPlayer = withRouter(Component)
