import styled from 'styled-components'
import { colors } from '../../../../assets/styles/colors'

export const Tabs = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  margin-top: 1em;
  
  > .headers {
    display: flex;
    
    > div {
      padding: 1em;
      background-color: ${colors.secondary};
      color: ${colors.white};
      flex: 1 1 auto;
      display: flex;
      justify-content: center;
      position: relative;
      
      > .icon {
        position: absolute;
        top: 0;
        right: 0;
        transform: translateY(50%);
        margin-right: 0.5em;
        display: none;
      }
      
      &.active {
        background-color: ${colors.darkSecondary};
        
        > .icon {
          display: block;
        }
      }
    }
  }
`

export const TabContent = styled.div`
  display: none;
  flex-direction: column;

  &.active {
    display: flex;
  }
`

export const PlayersContent = styled(TabContent)`
  position: relative;
`

export const MatchesContent = styled(TabContent)`

`
