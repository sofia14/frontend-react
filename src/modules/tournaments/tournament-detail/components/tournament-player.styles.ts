import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const TournamentPlayerWrapper = styled.div`
  display: flex;
  height: 100px;
  padding: 1em;
  background-color: ${colors.primary};
  color: ${colors.white};
  margin-top: 1em;
  width: 100%;
  
  > .player-info {
    flex: 0 1 auto;
    margin-left: 1em;
    display: flex;
    flex-direction: column;
    
    > div + div {
      margin-top: 0.2em;
    }
  }
  
  > .rank {
    flex: 1 1 auto;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    
    > div {
        &:last-child {
          font-size: 1.8em;
          margin-left: 0.5em;
        }
    }
  }
  
  > .image {
    flex: 0 1 auto;

    > img {
      max-width: 55px;
      max-height: 100%;
    }
  }
`
