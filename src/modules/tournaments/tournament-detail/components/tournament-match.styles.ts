import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const TournamentMatchWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100px;
  padding: 1em;
  background-color: #F2F5FF;
  color: ${colors.black};
  box-shadow: 0 0 5px #eee;
  margin-top: 1em;
  width: 100%;
  
  > .player {
    text-align: center;
    display: flex;
    justify-content: space-around;
    
    > div {
      font-weight: bold;
    
      &:last-child {
        color: ${colors.darkSecondary};
      
         &.winner {
          color: ${colors.green}
        }
      }
    }
    
    > .name {
        display: flex;
        justify-content: flex-start;
        flex: 1;
        font-weight: bold;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
  }
`
