import React, { FunctionComponent, useEffect, useState } from 'react'
import classNames from 'classnames'

import { TournamentMatchWrapper } from './tournament-match.styles'

export interface PlayerMatch {
    name: string
    surname: string
    rank: number
    result: number
}

export interface TournamentMatchProps {
    players: PlayerMatch[]
}

export const TournamentMatch: FunctionComponent<TournamentMatchProps> = (
    {
        players
    }
) => {
    const [playerOne, playerTwo] = players
    const [winnerIndex, setWinnerIndex] = useState(-1)

    useEffect(() => {
        const [first, second] = players
        const index = (first.result > second.result) ? 1 : 2

        setWinnerIndex((index))
    }, [players])

    const getResult = (result: number) => {
        return (result === undefined) ? '-' : result
    }

    return (
        <TournamentMatchWrapper>
            <div className="player">
                <div
                    className={classNames('name')}>
                    ({playerOne.rank}) {playerOne.name} {playerOne.surname}
                </div>

                <div className={classNames({ 'winner': winnerIndex === 1 })}>
                    {getResult(playerOne.result)
                    }</div>
            </div>

            <div className="player">
                <div
                    className={classNames('name')}>
                    ({playerTwo.rank}) {playerTwo.name} {playerTwo.surname}
                </div>

                <div className={classNames({ 'winner': winnerIndex === 2 })}>
                    {getResult(playerTwo.result)}
                </div>
            </div>
        </TournamentMatchWrapper>
    )
}
