import React, { FunctionComponent } from 'react'

import { TournamentWinnerWrapper } from './tournament-winner.styles'
import { getPlayerName } from '../../../players/players.utils'

export interface Winner {
    name?: string
    surname?: string
    photo?: string
}

export interface TournamentWinnerProps {
    winner: Winner
}

export const TournamentWinner: FunctionComponent<TournamentWinnerProps> = (
    {
        winner
    }
) => {
    return (
        <TournamentWinnerWrapper>
            <div>Winner</div>
            <div className="img-wrapper">
                <img
                    src={winner.photo || 'https://cdn1.vectorstock.com/i/thumbs/46/55/person-gray-photo-placeholder-woman-vector-22964655.jpg'}/>
            </div>

            <div className="name">{getPlayerName(winner)}</div>
        </TournamentWinnerWrapper>
    )
}
