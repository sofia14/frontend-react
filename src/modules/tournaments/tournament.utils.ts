export const getCountry = (country: string) => {
    if (!country) {
        return ''
    }

    return country.charAt(0).toUpperCase() + country.slice(1).toLowerCase()
}
