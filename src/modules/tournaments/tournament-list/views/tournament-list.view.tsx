import React, { FunctionComponent, useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'

import { FetchHook } from '../../../core/request/hooks.request'
import { fetchTournaments, ListTournament, TournamentListResponse } from '../tournament-list.api'
import { TournamentCard } from '../components/tournament-card/tournament-card'
import { ContentListContainer } from '../../../core/styles/container.styles'
import { setTitleAction } from '../../../core/store/ui/ui.actions'
import { connect } from 'react-redux'
import { Pagination } from '../../../../ui-kit/components/pagination/pagination'
import { FiltersTournament } from '../components/filters/filters-tournament'

export interface TournamentListProps extends RouteComponentProps {
    setTitle: typeof setTitleAction
}

const Component: FunctionComponent<TournamentListProps> = (
    {
        history,
        setTitle
    }
) => {
    const [search, setSearch] = useState('')
    const [year, selectYear] = useState(2020)

    const [{ data, isLoading }, setFilters] = FetchHook<TournamentListResponse>({
        fetchCallback: fetchTournaments,
        data: { tournaments: [], count: 0 },
        filters: { offset: 0, limit: 25, year: 2020 }
    })

    const [page, setPage] = useState(0)

    useEffect(() => {
        setFilters({ offset: page * 25, limit: 25, search, year })
    }, [page])

    useEffect(() => {
        setTitle('Tournament list')
    }, [])

    const onClick = (id: string) => {
        history.push(`/tournament/${id}`)
    }

    const onPage = (page: number) => {
        setPage(page)
    }

    return (
        <div>
            <FiltersTournament
                isLoading={isLoading}
                handleSearch={setSearch}
                handleYear={selectYear}
            />

            <ContentListContainer>
                {data.tournaments.map((tournament: ListTournament) => (
                    <TournamentCard
                        key={tournament.id}
                        handleClick={() => onClick(tournament.id)}
                        tournament={tournament}
                    />
                ))}

                <Pagination
                    count={data.count}
                    handleClick={onPage}
                    page={page}
                />
            </ContentListContainer>
        </div>
    )
}

const mapStateToProps = () => {
    return {}
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        setTitle: (title: string) => dispatch(setTitleAction(title))
    }
}

export const TournamentListView = connect(
    mapStateToProps,
    mapDispatchToProps
)(Component)
