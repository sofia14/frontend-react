import React, { FunctionComponent } from 'react'
import { RouteComponentProps } from 'react-router-dom'

import { ContentListContainer } from '../../../core/styles/container.styles'
import { fetchCurrentTournaments, ListTournament, TournamentListResponse } from '../tournament-list.api'
import { TournamentCard } from '../components/tournament-card/tournament-card'
import { FetchHook } from '../../../core/request/hooks.request'

export interface TournamentCurrentViewProps extends RouteComponentProps {
}

export const TournamentCurrentView: FunctionComponent<TournamentCurrentViewProps> = (
    {
        history
    }
) => {
    const [{ data }] = FetchHook<TournamentListResponse>({
        fetchCallback: fetchCurrentTournaments,
        data: { tournaments: [], count: 0 },
        filters: {}
    })

    const onClick = (id: string) => {
        history.push(`/tournament/${id}`)
    }

    return (
        <ContentListContainer>
            {data.tournaments.map((tournament: ListTournament) => (
                <TournamentCard
                    key={tournament.id}
                    handleClick={() => onClick(tournament.id)}
                    tournament={tournament}
                />
            ))}
        </ContentListContainer>
    )
}
