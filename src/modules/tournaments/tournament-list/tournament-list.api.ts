import { apiRequest } from '../../core/request/api.request'
import { Filters, toQueryString } from '../../../utils/request.utils'

export type Surface = 'Hard' | 'Grass' | 'Clay'

export interface ListTournament {
    id: string
    name: string
    surface: Surface
    year: number
    start: string
    end: string
    winner: any
    drawSingle: number
    level: string
    money: number
}

export interface TournamentListResponse {
    tournaments: ListTournament[]
    count: number
}

export interface TournamentListParams extends Filters {
    offset: number
    limit: number
}

export async function fetchTournaments(params: TournamentListParams): Promise<TournamentListResponse> {
    const query = toQueryString(params)
    const response = await apiRequest.get(`http://localhost:4002/api/v1/tournaments${query}`)

    return {
        tournaments: response.data || [],
        count: response.count || 0
    }
}

export async function fetchCurrentTournaments() {
    const response = await apiRequest.get(`http://localhost:4002/api/v1/tournaments/current`)

    return {
        tournaments: response.data || [],
        count: response.count || 0
    }
}
