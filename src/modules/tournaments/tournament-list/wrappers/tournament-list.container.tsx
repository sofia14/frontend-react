import React from 'react'
import { Redirect, Switch, useRouteMatch } from 'react-router-dom'

import { PrivateRoute } from '../../../../routing/private-route'
import { TournamentListView } from '../views/tournament-list.view'
import { SubMenu } from '../components/sub-menu/sub-menu'
import { TournamentCurrentView } from '../views/tournament-current.view'

export const TournamentListContainer = () => {
    const { path } = useRouteMatch()

    return (
        <div>
            <SubMenu/>

            <Redirect from={`${path}`} to={`${path}/current`}/>
            <Switch>
                <PrivateRoute path={`${path}/list`} component={TournamentListView}/>
                <PrivateRoute path={`${path}/current`} component={TournamentCurrentView}/>
            </Switch>
        </div>
    )
}
