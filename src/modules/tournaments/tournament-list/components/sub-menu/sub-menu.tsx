import React from 'react'
import { NavLink, useRouteMatch } from 'react-router-dom'

import { SubMenuWrapper } from './sub-menu.styles'

export const SubMenu = () => {
    const { path } = useRouteMatch()

    return (
        <SubMenuWrapper>
            <li>
                <NavLink to={`${path}/list`}>List</NavLink>
            </li>

            <li>
                <NavLink to={`${path}/current`}>Current</NavLink>
            </li>
        </SubMenuWrapper>
    )
}
