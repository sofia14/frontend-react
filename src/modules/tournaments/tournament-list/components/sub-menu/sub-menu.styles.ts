import styled from 'styled-components'
import { colors } from '../../../../../assets/styles/colors'

export const SubMenuWrapper = styled.ul`
  list-style-type: none;
  width: 100%;
  margin: 0;
  padding: 1em;
  display: flex;
  justify-content: center;
  
  > li + li {
    margin-left: 1em;
  }
  
  > li {
    flex: 1;
    text-align: center;
    background-color: #506a8c;
    border-radius: 3px;
    
    > a {
      display: block;
      padding: 0.5em 1.5em;
      color: ${colors.white};
      text-decoration: none;
      
      &.active {
           background-color: ${colors.primary};
      }
    }
  }
`
