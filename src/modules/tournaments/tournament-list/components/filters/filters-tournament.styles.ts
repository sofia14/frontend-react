import styled from 'styled-components'

import { colors } from '../../../../../assets/styles/colors'

export const FiltersTournamentWrapper = styled.div`

  .material-icons {
    color: ${colors.primary}
  }
`
