import React, { FunctionComponent, useState } from 'react'

import { TopBar } from '../../../../core/styles/container.styles'
import { SelectItem } from '../../../../../ui-kit/components/select/select'
import { Filters } from '../../../../core/components/filters/filters'
import { FiltersTournamentWrapper } from './filters-tournament.styles'

export interface FiltersProps {
    isLoading: boolean

    handleSearch(value: string): void

    handleYear(value: number): void
}

export const FiltersTournament: FunctionComponent<FiltersProps> = (
    {
        isLoading,
        handleSearch,
        handleYear
    }
) => {
    const [show, setShow] = useState(false)

    const setSearch = (value: string) => {
        handleSearch(value)
    }

    const selectYear = (value: number) => {
        handleYear(value)
    }

    const onSearch = (value: string) => {
        setSearch(value)
    }

    const onSelectYear = (year: SelectItem) => {
        selectYear(year.value as number)
    }

    const onClick = () => {
        setShow(!show)
    }

    return (
        <FiltersTournamentWrapper>
            <TopBar>
                <div className="icons">
                    <span
                        className="material-icons"
                        onClick={onClick}
                    >
                        filter_alt
                    </span>
                </div>

                {show ? <Filters
                    handleSearch={onSearch}
                    handleSelectYear={onSelectYear}
                    loading={isLoading}
                /> : null}
            </TopBar>
        </FiltersTournamentWrapper>
    )
}
