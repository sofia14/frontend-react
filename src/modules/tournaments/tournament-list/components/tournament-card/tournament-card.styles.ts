import styled from 'styled-components'

import { colors } from '../../../../../assets/styles/colors'

export const TournamentCardWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 300px;
    height: 450px;
    padding: 0.8em;
    border-radius: 15px;
    background: #3D5A80;
    text-align: left;
    box-shadow: 0 5px 20px -10px #111111;
    position: relative;
    cursor: pointer;
    margin: auto;
    
    @media (min-width: 768px) {
      margin-left: 2em;
    }
    
    &.card--hard {
      //background: linear-gradient(120deg, #1CB5E0 0%, #000851 100%);
      //box-shadow: 0 5px 20px -10px #000851;
    }
    
    &.card--hard .type {
      background-color: #1CB5E0;
    }
    
    &.card--clay {
      //background: linear-gradient(110deg, #FDBB2D 0%, #3A1C71 100%);
      //box-shadow: 0 5px 20px -10px #3A1C71;
    }
    
    &.card--clay .type {
      background-color: #C08A53;
    }
    
    &.card--grass {
      background: linear-gradient(140deg, #c4da3d 0%, #6e7f0e 69%, #275009 100%);
    }
    
    &.card--grass .type {
      background: #6e7f0e;
    }

`

export const ImageContainer = styled.div`
    transition: transform 0.4s;
    background-color: #fff;
    text-align: center;
    padding: 0.3em 0.3em 0;
    border-radius: 3px 3px 0 0;
    height: 125px;
    
    > img {
      width: 100%;
    }
`

export const CardCaption = styled.figcaption`
  flex: 1;

  > div {
    height: 100%;
    display: flex;
    flex-direction: column;
  
    > .name {
        font-family: "Open Sans Condensed", "Open Sans", helvetica, sans-serif;
        text-align: center;
        font-size: 1.5em;
        font-weight: 700;
        letter-spacing: 0.02em;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
        color: ${colors.white};
        margin-top: 0.5em;
      }
  }
  
  > .type {
    position: absolute;
    top: 0;
    right: 1em;
    transform: translateY(-50%);
    color: #ffffff;
    text-transform: uppercase;
    font-family: "Open Sans Condensed", "Open Sans", helvetica, sans-serif;
    letter-spacing: 0.1em;
    padding: 0.25em;
    line-height: 1;
    border-radius: 2px;
    background: #bbbbbb;
  }
`

export const CardStats = styled.div`
    margin: 1em 0 0 0;
    width: 100%;
    padding: 0 1em;
    color: ${colors.white};
    display: flex;
    flex-direction: column;
    flex: 1;
    
    > tbody {         
        th, td {
            width: 50%;
            padding: 0.25em 0;
        }
        
        td {
          padding-left: 1em;
        }
    }
`
export const CardBottom = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: auto;
  
  > .info {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    margin-top: 1em;
    flex: 1 0;
    
    &:last-child {
      text-align: right ;
    }
    
    > .label {
        font-size: 10px;
        text-transform: uppercase;
        font-weight: 400;
        display: block;
        margin-bottom: 3px;
    }
  }
`
