import React, { FunctionComponent } from 'react'

import { ListTournament } from '../../tournament-list.api'
import { CardBottom, CardCaption, CardStats, ImageContainer, TournamentCardWrapper } from './tournament-card.styles'
import { formatMoney } from '../../../../../utils/money.utils'
import { getDate } from '../../../../../utils/date.utils'

export interface TournamentCardProps {
    handleClick: () => void
    tournament: ListTournament
}

export const TournamentCard: FunctionComponent<TournamentCardProps> = (
    {
        handleClick,
        tournament
    }
) => {

    const getSurfaceClass = () => {
        return tournament.surface ? tournament.surface.toLowerCase() : ''
    }

    const getImage = () => {
        const surface = getSurfaceClass()
        return `/images/surfaces/tournament-${surface}.jpg`
    }

    const getWinnerName = (winner: any) => {
        return `${winner.name.charAt(0)}. ${winner.surname}`
    }

    return (
        <TournamentCardWrapper
            className={'card card--' + getSurfaceClass()}
            onClick={handleClick}
        >
            <ImageContainer>
                <img
                    src={getImage()}
                    alt="Tournament logo"
                    className="card__image"/>
            </ImageContainer>

            <CardCaption>
                <h3 className="type">
                    {tournament.surface || '-'}
                </h3>

                <div>
                    <h1 className="name">{tournament.name}</h1>

                    <CardStats>
                        <tbody>
                        <tr>
                            <th>Year</th>
                            <td>{tournament.year || '-'}</td>
                        </tr>

                        <tr>
                            <th>Start</th>
                            <td>{getDate(tournament.start)}</td>
                        </tr>

                        <tr>
                            <th>End</th>
                            <td>{getDate(tournament.end)}</td>
                        </tr>

                        <tr>
                            <th>Winner</th>
                            <td>{tournament.winner ? getWinnerName(tournament.winner) : '-'}</td>
                        </tr>

                        <tr>
                            <th>Past winner</th>
                            <td>-</td>
                        </tr>

                        <tr>
                            <th>Draw size</th>
                            <td>{tournament.drawSingle || '-'}</td>
                        </tr>
                        </tbody>

                        <CardBottom>
                            <h4 className="info">
                                <span className="label">Level</span>
                                {tournament.level || '-'}
                            </h4>
                            <h4 className="info">
                                <span className="label">Prize pool</span>
                                {tournament.money ? formatMoney(tournament.money, 'us', 'USD') : '-'}
                            </h4>
                        </CardBottom>
                    </CardStats>
                </div>
            </CardCaption>
        </TournamentCardWrapper>
    )
}
