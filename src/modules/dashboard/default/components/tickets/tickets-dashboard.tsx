import React from 'react'

import { TicketsDashboardWrapper } from './tickets-dashboard.styles'
import { TicketSimple } from '../../../../tickets/components/ticket-simple/ticket-simple'

export const TicketsDashboard = () => {
    return (
        <TicketsDashboardWrapper>
            <div className="tickets">
                <TicketSimple
                    deposit={1000}
                    income={2000}
                    status="Evaluated"
                />

                <TicketSimple
                    deposit={1000}
                    income={-2000}
                    status="Evaluated"
                />

                <TicketSimple
                    deposit={1000}
                    income={0}
                    status="Pending"
                />

                <TicketSimple
                    deposit={1000}
                    income={0}
                    status="Cancelled"
                />
            </div>
        </TicketsDashboardWrapper>
    )
}
