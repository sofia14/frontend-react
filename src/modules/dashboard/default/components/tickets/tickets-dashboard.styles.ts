import styled from 'styled-components'

export const TicketsDashboardWrapper = styled.div`
  width: 100%;
  
  > .tickets {
    display: flex;
    flex-direction: column;
    
    > div + div {
      margin-top: 1em;
    }
  }
`
