import React, { FunctionComponent, useEffect } from 'react'
import { connect } from 'react-redux'

import { ContentListContainer } from '../../core/styles/container.styles'
import { setTitleAction } from '../../core/store/ui/ui.actions'

export interface DashboardViewProps {
    setTitle: typeof setTitleAction
}

const Component: FunctionComponent<DashboardViewProps> = (
    {
        setTitle
    }
) => {
    useEffect(() => {
        setTitle('Dashboard')
    }, [])

    return (
        <ContentListContainer>
            xxxx
        </ContentListContainer>
    )
}

const mapStateToProps = () => {
    return {}
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        setTitle: (title: string) => dispatch(setTitleAction(title))
    }
}

export const DashboardView = connect(
    mapStateToProps,
    mapDispatchToProps
)(Component)
