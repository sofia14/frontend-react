import React, { FunctionComponent, useEffect, useState } from 'react'

import { IncomeWrapper } from '../ticket/ticket.styles'
import { TicketStatus } from '../ticket/ticket'

export interface IncomeProps {
    income: number
    status: TicketStatus
}

export const Income: FunctionComponent<IncomeProps> = (
    {
        income,
        status
    }
) => {
    const [incomeType, setIncomeType] = useState('')

    useEffect(() => {
        if (status == 'Evaluated') {
            setIncomeType((income > 0) ? 'won' : 'lost')
        } else if (status == 'Cancelled') {
            setIncomeType('cancelled')
        }
    }, [status])

    return (
        <IncomeWrapper className={incomeType}>
            <div>{income} CZK</div>
        </IncomeWrapper>
    )
}
