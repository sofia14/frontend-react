import React, { FunctionComponent } from 'react'

import { TicketSimpleWrapper } from './ticket-simple.styles'
import { TicketStatus } from '../ticket/ticket'
import { CreatedAt, Status } from '../ticket/ticket.styles'
import { Income } from './income'

export interface TicketSimpleProps {
    status: TicketStatus
    deposit: number
    income: number
}

export const TicketSimple: FunctionComponent<TicketSimpleProps> = (
    {
        status,
        income,
        deposit
    }
) => {
    return (
        <TicketSimpleWrapper>
            <div className="top">
                <Status>
                    <div>{status}</div>
                </Status>

                <CreatedAt>
                    <div>18.9.2020</div>
                </CreatedAt>
            </div>

            <Income
                income={income}
                status={status}
            />
        </TicketSimpleWrapper>
    )
}
