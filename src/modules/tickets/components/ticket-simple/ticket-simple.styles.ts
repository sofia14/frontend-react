import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const TicketSimpleWrapper = styled.div`
    padding: 1em;
    background-color: ${colors.primary};
    color: ${colors.white};
    width: 100%;
    
    > .top {
      display: flex;
      justify-content: space-between;
      margin-right: 1em;
    }
`
