import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const TicketWrapper = styled.div`    
    padding: 1em;
    background-color: ${colors.primary};
    color: ${colors.white};
    width: 100%;
    
    > .top {
      display: flex;
      justify-content: space-between;
      margin-bottom: 1em;
    }

    > .tips {
      > div + div {
        margin-top: 0.5em;
      }
    }
`

export const Status = styled.div`
  display: flex;

  > div + div {
    margin-left:  0.5em;
  }
  
  > div {
    &:last-child {
      font-weight: bold;
    }
  }
`

export const Money = styled(Status)`
`

export const CreatedAt = styled.div`

`

export const IncomeWrapper = styled.div`
  color: ${colors.white};
  padding: 0.5em 1em;
  margin-top: 1em;
  background-color: ${colors.orange};
  display: flex;
  justify-content: center;
  align-items: center;
  
  &.won {
    background-color: ${colors.green};
  }
  
  &.lost {
    background-color: ${colors.darkSecondary};
  }
  
  &.cancelled {
   background-color: ${colors.grey};
  }
`
