import React, { FunctionComponent, useEffect, useState } from 'react'

import { TicketTipWrapper } from './ticket-tip.styles'

export type TipStatus = 'lost' | 'win' | 'waiting'

export interface TicketTipProps {
    status: TipStatus
    date: string
    tournament: string
    tip: string
}

export const TicketTip: FunctionComponent<TicketTipProps> = (
    {
        status,
        date,
        tournament,
        tip
    }
) => {
    const [icon, setIcon] = useState('')
    const [iconColor, setIconColor] = useState('')

    useEffect(() => {
        let icon = 'hourglass_empty'
        let color = 'grey'

        if (status == 'lost') {
            icon = 'cancel'
            color = 'red'
        } else if (status == 'win') {
            icon = 'done'
            color = 'green'
        }

        setIcon(icon)
        setIconColor(color)
    }, [status])

    return (
        <TicketTipWrapper>
            <div className="top">
                <div>{date}</div>
                <div>{tournament}</div>
            </div>

            <div className="tip">
                <div>{tip}</div>

                <span className={'material-icons icon' + ' ' + iconColor}>
                    {icon}
                </span>
            </div>

        </TicketTipWrapper>
    )
}
