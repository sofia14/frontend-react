import styled from 'styled-components'

import { colors } from '../../../../assets/styles/colors'

export const TicketTipWrapper = styled.div`
  background-color: #597fb0;
  color: ${colors.white};
  padding: 0.5em 1em;

  > .top,
   .tip {
    display: flex;
    justify-content: space-between;
  }
  
  > .tip {
     display: flex;
     align-items: center;
  
      > .icon {
      
        &.grey {
            color: ${colors.grey};
        }
        
        &.red {
          color: ${colors.darkSecondary};
        }
        
        &.green {
          color: ${colors.green};
        }
      }
  }
`
