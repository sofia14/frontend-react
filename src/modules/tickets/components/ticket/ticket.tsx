import React, { FunctionComponent } from 'react'

import { Money, Status, TicketWrapper } from './ticket.styles'
import { TicketTip } from './ticket-tip'
import { Income } from '../ticket-simple/income'

export type TicketStatus = 'Pending' | 'Evaluated' | 'Cancelled'

export interface TicketProps {
    status: TicketStatus
    deposit: number
    income: number
}

export const Ticket: FunctionComponent<TicketProps> = (
    {
        deposit,
        income,
        status
    }
) => {

    return (
        <TicketWrapper>
            <div className="top">
                <Status>
                    <div>Status:</div>
                    <div>{status}</div>
                </Status>

                <Money>
                    <div>Deposit:</div>
                    <div>{deposit} CZK</div>
                </Money>
            </div>

            <div className="tips">
                <TicketTip
                    date="28.4.2020"
                    status="win"
                    tip="Win - Dodin Oceane"
                    tournament="Australian Open"
                />

                <TicketTip
                    date="28.4.2020"
                    status="lost"
                    tip="Win - Dodin Oceane"
                    tournament="Australian Open"
                />

                <TicketTip
                    date="28.4.2020"
                    status="waiting"
                    tip="Win - Dodin Oceane"
                    tournament="Australian Open"
                />
            </div>

            <Income
                income={income}
                status={status}
            />
        </TicketWrapper>
    )
}
