export const colors = {
    primary: '#3D5A80',
    secondary: '#EE6C4D',
    darkSecondary: '#be563d',
    white: '#fff',
    black: '#293241',
    green: '#5cb85c',
    red: '#d9534f',
    orange: '#ff8c00',
    grey: '#a39cb0'
}
