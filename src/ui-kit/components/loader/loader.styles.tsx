import styled from 'styled-components'
import { colors } from '../../../assets/styles/colors'

export const LoaderWrapper = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 999999;

  > .loader {
    position: relative;
    width: 60px;
    height: 60px;
    border-radius: 50%;
    margin: 75px;
    display: inline-block;
    vertical-align: middle;

    > .loader-outter {
      position: absolute;
      border: 4px solid ${colors.primary};
      border-left-color: transparent;
      border-bottom: 0;
      width: 100%;
      height: 100%;
      border-radius: 50%;
      animation: loader-outter 1s cubic-bezier(.42, .61, .58, .41) infinite;
    }

    > .loader-inner {
      position: absolute;
      border: 4px solid ${colors.primary};
      border-radius: 50%;
      width: 40px;
      height: 40px;
      left: calc(50% - 20px);
      top: calc(50% - 20px);
      border-right: 0;
      border-top-color: transparent;
      animation: loader-inner 1s cubic-bezier(.42, .61, .58, .41) infinite;
    }

    &.loader--white {
      > .loader-outter,
      > .loader-inner {
        border: 4px solid ${colors.white};
        border-right: 0;
        border-top-color: transparent;
      }
    }
  }
  
  &.small {
     > .loader {
        width: 30px;
        height: 30px;
        margin: 0;
        
        > .loader-inner {
            width: 20px;
            height: 20px;
            left: calc(50% - 10px);
            top: calc(50% - 10px);
        }
     }
  }

  @keyframes loader-outter {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }

  @keyframes loader-inner {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(-360deg);
    }
  }
`
