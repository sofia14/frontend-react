import React, { FunctionComponent } from 'react'
import classnames from 'classnames'

import { LoaderWrapper } from './loader.styles'

export type LoaderVariant = 'white' | 'blue'

export interface LoaderProps {
    variant?: LoaderVariant
    small?: boolean
}

export const Loader: FunctionComponent<LoaderProps> = (
    {
        variant = 'black',
        small
    }
) => {
    return (
        <LoaderWrapper className={classnames({ 'small': small })}>
            <div className={`loader loader--${variant}`}>
                <div className="loader-outter"/>
                <div className="loader-inner"/>
            </div>
        </LoaderWrapper>
    )
}
