import styled from 'styled-components'
import { colors } from '../../../assets/styles/colors'

export const SelectWrapper = styled.div`
    width: 100%;
    position: relative;
    font-size: 1.1em;
    height: 64px;
    
    >  ul {
        list-style-type: none;
        padding-left: 0;
    }
    
    .options {
        z-index: 99999;
        width: 100%;
        position: absolute;
        border: 1px solid ${colors.primary};
        background-color: #fff;
        margin: 0.2em 0 0 0;
    }
    
    .option {
        padding: 1em;
        cursor: pointer;
    }
    
    .options.hidden {
      visibility: hidden;
    }
    
    .options .option:hover {
        background-color: ${colors.primary};
        color: #fff;
    }
    
    .selected {
        display: flex;
        align-items: center;
        border: 4px solid ${colors.primary};
        background-color: #fff;
        padding: 0.8em 1.4em;
        height: 64px;
    }
`
