import React, { FunctionComponent, useState } from 'react'

import { SelectWrapper } from './select.styles'

export interface SelectItem {
    value: string | number
    label: string
}

export interface SelectProps {
    placeholder: string

    handleSelected(item: SelectItem): void

    options: any[]
    selected: any
}

export const Select: FunctionComponent<SelectProps> = (
    {
        placeholder,
        handleSelected,
        options,
        selected
    }
) => {
    const [hidden, setHidden] = useState(true)

    const length = options.length

    const onClick = () => {
        if (length > 0) {
            setHidden(!hidden)
        }
    }

    const onSelect = (item: SelectItem) => {
        handleSelected(item)
        setHidden(true)
    }

    return (
        <SelectWrapper>
            <div className="selected option" onClick={onClick}>{selected?.label || placeholder}</div>
            <ul className={'options ' + (hidden ? 'hidden' : '')}>
                {options.map(option => (
                    <li
                        key={option.value}
                        className="option"
                        onClick={() => onSelect(option)}>{option.label}</li>
                ))}
            </ul>
        </SelectWrapper>
    )
}
