import styled from 'styled-components'

export const InputWrapper = styled.div`
    > label {
      color: #2E7B80
    }

    > input {
        outline: none;
        font-size: 1.1em;
        background: #fff;
        width: 100%;
        padding: 0.8em 1.4em;
        border: none;
        border-bottom: 2px solid #43B0B7;
    }
`
