import React, { ChangeEvent, FunctionComponent } from 'react'

import { InputWrapper } from './input.styles'

export type InputType = 'text' | 'password' | 'email'

export interface InputParams {
    type: InputType
    handleChange: (value: string) => void
    value: string | number
    label?: string
    placeholderInner?: string
}

export const Input: FunctionComponent<InputParams> = (
    {
        type,
        label,
        handleChange,
        value,
        placeholderInner = ''
    }
) => {

    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value
        handleChange(value)
    }

    return (
        <InputWrapper>
            {label ? <label>{label}</label> : null}
            <input
                placeholder={placeholderInner}
                type={type}
                onChange={onChange}
                value={value}
            />
        </InputWrapper>
    )
}
