import styled from 'styled-components'

export const SectionTopWrapper = styled.div`
  padding: 0.5em 0.5em 1em 0.5em;
  margin-bottom: -1em;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  flex-direction: column;
  background: linear-gradient(to left, #597fb0, #3d5a80);
  color: #fff;
  
  > span {
    position: absolute;
    left: 0.5em ;
    top: 0.5em;
  }
`
