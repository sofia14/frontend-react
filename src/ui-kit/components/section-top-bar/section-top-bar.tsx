import React, { FunctionComponent } from 'react'

import { SectionTopWrapper } from './section-top-bar.styles'

export interface SectionTitleProps {
    handleClick: () => void
}

export const SectionTopBar: FunctionComponent<SectionTitleProps> = (
    {
        children,
        handleClick,
    }
) => {
    return (
        <SectionTopWrapper onClick={handleClick}>
            <span className="material-icons">
                arrow_back
            </span>
            {children}
        </SectionTopWrapper>
    )
}
