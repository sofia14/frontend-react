import styled from 'styled-components'
import { colors } from '../../../assets/styles/colors'

export const MobileMenuBottomWrapper = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  z-index: 1;
  
  background-color: ${colors.primary};
  
  > ul {
    display: flex;
    height: 100%;
  
    list-style-type: none;
    padding: 0.5em 1em;
    margin: 0;
    
    > li {      
      & + li {
        margin-left: 1em;
      }
    
      > a {
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        align-items: center;
      
        color: ${colors.white};
        text-decoration: none;
        
        > img {
          filter: invert(1);
        }
        
        > div {
          margin-top: 0.1em;
          font-size: 0.9em;
        }
      }
    }
  }
`
