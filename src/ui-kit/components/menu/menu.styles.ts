import styled from 'styled-components'
import { colors } from '../../../assets/styles/colors'

export const MenuWrapper = styled.div`
    display: flex;
    align-items: center;
    padding: 0.5em 1em;
    height: 3.5em;
    background-color: ${colors.primary};
    color: ${colors.white};
    position: relative;
    justify-content: space-between;
    
    > .logo {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: space-between;
      height: 100%;
      
      > div {
        font-size: 0.9em;
      }
      
      > img {
        filter: invert(1);
      };
    }
    
    > .material-icons {
      font-size: 2em;
    }
`
