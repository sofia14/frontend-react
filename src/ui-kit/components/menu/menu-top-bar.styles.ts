import styled from 'styled-components'
import { colors } from '../../../assets/styles/colors'

export const Title = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  
  > div {
    &:last-child {
      color: ${colors.green};
      margin-top: 0.1em;
    }
  }
`
