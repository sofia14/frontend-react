import React, { FunctionComponent, useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import classnames from 'classnames'

import { MenuContent, MobileMenuWrapper } from './mobile-menu.styles'
import PLayerLogo from './icons/tennis.svg'
import TournamentLogo from './icons/tournament.svg'
import DashboardLogo from './icons/dashboard.svg'
import LogoutLogo from './icons/logout.svg'
import DownloadLogo from './icons/download.svg'
import { getUserName } from '../../../modules/user/utils/user.utils'

export interface MobileMenuProps {
    handleClose(): void

    handleMenuItemClick(): void

    handleLogout(): void

    open: boolean
}

export const MobileMenu: FunctionComponent<MobileMenuProps> = (
    {
        handleClose,
        handleMenuItemClick,
        handleLogout,
        open
    }
) => {

    const [username, setUsername] = useState('')

    useEffect(() => {
        const user = getUserName()
        setUsername(user)
    }, [])

    return (
        <MobileMenuWrapper className={classnames({ 'hidden': !open })}>
             <span
                 onClick={handleClose}
                 className="icon material-icons">
                close
            </span>

            <div className="top">
                <div className="img">
                    <img src="https://praguebusinessjournal.com/wp-content/uploads/2017/11/zeman-idiot.jpg"/>
                </div>

                <div className="username">{username}</div>

                <div className="quick-menu">
                    <a href="#">
                        <div className="dot"/>
                        <div>Profile</div>
                    </a>
                    <a href="#">
                        <div className="dot"/>
                        <div>Help</div>
                    </a>
                </div>
            </div>

            <MenuContent>
                <NavLink to="/dashboard" onClick={handleMenuItemClick}>
                    <img src={DashboardLogo} width={30}/>
                    <div>Dashboard</div>
                </NavLink>

                <NavLink to="/download" onClick={handleMenuItemClick}>
                    <img src={DownloadLogo} width={30}/>
                    <div>Download</div>
                </NavLink>

                <NavLink to="/tournaments" onClick={handleMenuItemClick}>
                    <img src={TournamentLogo} width={30}/>
                    <div>Tournaments</div>
                </NavLink>

                <NavLink to="/players" onClick={handleMenuItemClick}>
                    <img src={PLayerLogo} width={30}/>
                    <div>Players</div>
                </NavLink>

                <a onClick={handleLogout}>
                    <img src={LogoutLogo} width={30}/>
                    <div>Logout</div>
                </a>
            </MenuContent>
        </MobileMenuWrapper>
    )
}
