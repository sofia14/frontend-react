import React, { FunctionComponent, useState } from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'

import { MenuWrapper } from './menu.styles'
import { MenuTopBar } from './menu-top-bar'
import { MobileMenu } from './mobile-menu'
import { authApi } from '../../../modules/auth/auth.api'
import { MobileMenuBottom } from './mobile-menu-bottom'

export interface MenuProps extends RouteComponentProps {

}

const Component: FunctionComponent<MenuProps> = (
    {
        history,
    }
) => {
    const [open, setOpen] = useState(false)

    const onToggle = () => {
        setOpen(!open)
    }

    const onLogout = () => {
        authApi.logout()
        history.push('/login')
    }

    return (
        <MenuWrapper>
            <MenuTopBar
                handleOpen={onToggle}
            />

            <MobileMenu
                open={open}
                handleClose={onToggle}
                handleMenuItemClick={onToggle}
                handleLogout={onLogout}
            />

            <MobileMenuBottom/>
        </MenuWrapper>
    )
}

export const Menu = withRouter(Component)
