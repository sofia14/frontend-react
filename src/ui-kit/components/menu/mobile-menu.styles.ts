import styled from 'styled-components'

import { colors } from '../../../assets/styles/colors'

export const MobileMenuWrapper = styled.div`
  position: fixed;
  background-color: ${colors.white};
  width: 100%;
  height: 100vh;
  z-index: 10000;
  
  left: 0;
  top: 0;
  
  &.hidden {
    display: none;
  }
  
  > .icon {
    position: absolute;
    right: 0;
    top: 0;
    font-size: 2em;
    padding: 0.5em;
    z-index: 11;
  }
  
  > .top {
    background-color: ${colors.primary};
    height: 12em;
    border-bottom-left-radius: 15px;
    border-bottom-right-radius: 15px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    flex-direction: column;
    
    > .username {
      margin: 0.5em 0;
    }
    
    > .quick-menu {
      position: absolute;
      bottom: -20px;
      margin: 0 auto;
      background-color: ${colors.white};
      box-shadow: 0 0 2px ${colors.black};
      border-radius: 0.8em;
      width: 80vw;
      display: flex;
      justify-content: center;
      
      > a {
        color: ${colors.black};
        text-decoration: none;
        padding: 0.8em 1em;
        display: flex;
        align-items: center;
        line-height: 1.5;
        
        > .dot {
          height: 5px;
          width: 5px;
          margin-right: 0.5em;
          background: ${colors.secondary};
          margin-bottom: 0.1em;
        }
      }
    }
    
    > .img {
      height: 100px;
      border-radius: 50%;
      
      > img {
        height: 100%;
        border-radius: 50%;
        box-shadow: 0 0 2px ${colors.white};
      }
    }
  }
`

export const MenuContent = styled.div`
  padding: 4em 0 2em 0;

  > a + a {
    margin-top: 0.5em;
  }

  > a {
    display: flex;
    text-decoration: none;
    color: ${colors.black};
    align-items: center;
    padding: 1em 2em;
    
    &.active {
      background-color: #597fb0;
      color: ${colors.white};
    }
    
    > div {
      margin-left: 2em;
    }
  }
`
