import React, { Fragment, FunctionComponent } from 'react'
import { connect } from 'react-redux'

import Logo from '../../../assets/images/logo.svg'
import { IApplicationState } from '../../../store'
import { uiSelectors } from '../../../modules/core/store/ui/ui.selectors'
import { Title } from './menu-top-bar.styles'

export interface MenuTopBarProps {
    title: string

    handleOpen(): void
}

const Component: FunctionComponent<MenuTopBarProps> = (
    {
        title,
        handleOpen
    }
) => {
    return (
        <Fragment>
              <span
                  onClick={handleOpen}
                  className="material-icons">
                menu
            </span>

            <Title>
                <div>{title}</div>
            </Title>

            <div className="logo">
                <img src={Logo} height={20} alt="logo"/>
                <div>1000 CZK</div>
            </div>
        </Fragment>
    )
}

const mapDispatchToProps = () => {
    return {}
}

const mapStateToProps = (state: IApplicationState) => {
    return {
        title: uiSelectors.selectTitle(state)
    }
}

export const MenuTopBar = connect(
    mapStateToProps,
    mapDispatchToProps
)(Component)
