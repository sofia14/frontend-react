import React, { FunctionComponent } from 'react'

import { MobileMenuBottomWrapper } from './mobile-menu-bottom.styles'
import { NavLink } from 'react-router-dom'
import MatchLogo from './icons/matches.svg'
import TicketLogo from './icons/ticket.svg'
import LogsLogo from './icons/logs.svg'

export interface MobileMenuBottomProps {
}

export const MobileMenuBottom: FunctionComponent<MobileMenuBottomProps> = (
    {}
) => {

    return (
        <MobileMenuBottomWrapper>
            <ul>
                <li>
                    <NavLink to="/matches">
                        <img src={MatchLogo} width={30}/>
                        <div>Matches</div>
                    </NavLink>
                </li>

                <li>
                    <NavLink to="/tickets">
                        <img src={TicketLogo} width={30}/>
                        <div>Tickets</div>
                    </NavLink>
                </li>

                <li>
                    <NavLink to="/logs">
                        <img src={LogsLogo} width={30}/>
                        <div>Logs</div>
                    </NavLink>
                </li>
            </ul>
        </MobileMenuBottomWrapper>
    )
}
