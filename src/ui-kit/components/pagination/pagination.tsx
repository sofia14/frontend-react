import React, { FunctionComponent, useState } from 'react'
import classnames from 'classnames'

import { PaginationWrapper } from './pagination.styles'

export interface PaginationProps {
    count: number
    perPage?: number
    page: number

    handleClick(page: number): void
}

export const Pagination: FunctionComponent<PaginationProps> = (
    {
        count,
        perPage = 25,
        page,
        handleClick,
    },
) => {

    const [maxPage, setMaxPage] = useState(0)
    const [side, setSide] = useState('')

    const onPage = (side: string, pageChange: number) => {
        setSide(side)
        handleClick(page + pageChange)

        setTimeout(() => {
            setSide('')
        }, 500)
    }

    return (
        <PaginationWrapper>
            <div
                className={classnames('pagination-wrapper', { [side]: true, hidden: (count <= perPage) })}>
                <svg
                    onClick={() => onPage('transition-prev', -1)}
                    className={classnames('btn btn--prev', { 'disabled': (page == 0) })}
                    height="64"
                    viewBox="0 0 24 24"
                    width="64"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/>
                    <path d="M0-.5h24v24H0z" fill="none"/>
                </svg>

                <div className="pagination-container">
                    <div className="little-dot little-dot--first"/>
                    <div className="little-dot">
                        <div className="big-dot-container">
                            <div className="big-dot"/>
                        </div>
                    </div>
                    <div className="little-dot little-dot--last"/>
                </div>

                <svg
                    onClick={() => onPage('transition-next', 1)}
                    className={classnames('btn btn--next', { 'disabled': (page == maxPage - 1) })}
                    height="64"
                    viewBox="0 0 24 24"
                    width="64"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"/>
                    <path d="M0-.25h24v12H0z" fill="none"/>
                </svg>
            </div>
        </PaginationWrapper>
    )
}
