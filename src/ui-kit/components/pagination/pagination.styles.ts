import styled from 'styled-components'
import { colors } from '../../../assets/styles/colors'

export const PaginationWrapper = styled.div`
    margin-top: 1em;
    position: sticky;
    bottom: 0;
    z-index: 1000;
  
    > div {
        &.hidden {
          display: none;
        }
        
        font-size: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        
        @keyframes pagination-container--animation-prev {
            0% {
              transform: translateX(0);
            }
    
            100% {
              transform: translateX(18px);
            }
        }
        
        @keyframes pagination-container--animation-next {
            0% {
              transform: translateX(0);
            }
    
            100% {
              transform: translateX(-18px);
            }
        }
        
        .transition-prev .pagination-container {
          animation: pagination-container--animation-prev 0.3s forwards;
        }
        
        .transition-next .pagination-container {
          animation: pagination-container--animation-next 0.3s forwards;
        }
        
        .little-dot {
            width: 6px;
            height: 6px;
            background: #000;
            border-radius: 100%;
            display: inline-block;
            vertical-align: middle;
            margin: 0 6px;
            position: relative;
            z-index: 10;
        }
        
        .little-dot--first,
        .little-dot--last {
          z-index: 5;
        }
        
        @keyframes slideLeft {
            0% {
              transform: translateX(0px);
            }
            
            100% {
              transform: translateX(-18px);
            }
        }
        
        .transition-prev .little-dot--first {
          animation: slideLeft 0.4s 0.3s forwards cubic-bezier(0.165, 0.84, 0.44, 1);
        }
        
        @keyframes little-dot--first--animation {
            0% {
              opacity: 1;
            }
    
            100% {
              opacity: 0;
            }
        }
        
        .transition-next .little-dot--first {
          animation: little-dot--last--animation 0.3s forwards;
        }
        
        @keyframes little-dot--last--animation {
            0% {
              opacity: 1;
            }
    
            100% {
              opacity: 0;
            }
        }
        
        .transition-prev .little-dot--last {
          animation: little-dot--last--animation 0.3s forwards;
        }
        
        @keyframes slideRight {
            0% {
              transform: translateX(0px);
              opacity: 1;
            }
            
            100% {
              transform: translateX(18px);
              opacity: 1;
            }
        }
        
        .transition-next .little-dot--last {
          animation: slideRight 0.4s 0.3s forwards cubic-bezier(0.165, 0.84, 0.44, 1);
        }
        
        .big-dot {
            width: 12px;
            height: 12px;
            border-radius: 100%;
            background: ${colors.black};
            position: absolute;
            top: 50%;
            right: -6px;
            transform: translateY(-50%);
        }
        
        .transition-next .big-dot {
            right: auto;
            left: -6px;
        }
        
        .big-dot-container {
            width: 18px;
            height: 18px;
            border-radius: 100%;
            position: absolute;
            top: 50%;
            right: 3px;
            transform: translateY(-50%);
            z-index: 10;
        }
        
        .transition-next .big-dot-container {
            right: auto;
            left: 3px;
        }
        
        @keyframes big-dot-container--animation-prev {
            0% {
              transform: translateY(-50%);
            }
            100% {
              transform: translateY(-50%) rotate(-179deg);
            }
        }
        
        @keyframes big-dot-container--animation-next {
            0% {
              transform: translateY(-50%);
            }
            100% {
              transform: translateY(-50%) rotate(-181deg);
            }
        }
        
        .transition-prev .big-dot-container {
          animation: big-dot-container--animation-prev 0.3s forwards;
        }
        
        .transition-next .big-dot-container {
          animation: big-dot-container--animation-next 0.3s forwards;
        }
        
        .btn {
            fill: ${colors.black};
            cursor: pointer;
            transition: opacity 0.2s;
            
          &.disabled {
              pointer-events: none;
              opacity: 0.6;
            }
        }
        
        .btn:hover {
          opacity: 0.6;
        }
    }
`
