import styled from 'styled-components'

import { colors } from '../../../assets/styles/colors'

export const PaginationSimpleWrapper = styled.div`
  margin: 1em auto 0 auto;

  > ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
    display: flex;
    
    > li + li {
      margin-left: 0.5em;
    }
    
    > li {
        width: 3em;
        height: 3em;
        border: 2px solid ${colors.primary};
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: ${colors.primary};
        color: ${colors.white};
        
        &.disabled {
          pointer-events: none;
          opacity: 0.6;
        }
    }
  }
`
