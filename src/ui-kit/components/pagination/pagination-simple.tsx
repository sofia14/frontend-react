import React, { FunctionComponent, useEffect, useState } from 'react'
import classNames from 'classnames'

import { PaginationSimpleWrapper } from './pagination-simple.styles'

export interface PaginationSimpleProps {
    count: number
    perPage: number
    page: number

    handlePageChange (page: number): void
}

export const PaginationSimple: FunctionComponent<PaginationSimpleProps> = (
    {
        perPage,
        page,
        count,
        handlePageChange,
    },
) => {
    const [leftState, setLeftState] = useState(false)
    const [rightState, setRightState] = useState(false)

    useEffect(() => {
        const leftState = isLeftDisabled()
        const rightState = isRightDisabled()


        setLeftState(leftState)
        setRightState(rightState)
    }, [count, perPage, page])

    function isRightDisabled () {
        console.log(page * perPage, count)
        return (page * perPage) >= count
    }

    function isLeftDisabled () {
        return page == 0
    }

    function onPageChange (page: number) {
        handlePageChange(page)
    }

    return (
        <PaginationSimpleWrapper>
            <ul>
                <li
                    onClick={() => onPageChange(-1)}
                    className={classNames({ 'disabled': leftState })}>
                    <span className="material-icons">
                        keyboard_arrow_left
                    </span>
                </li>

                <li
                    onClick={() => onPageChange(1)}
                    className={classNames({ 'disabled': rightState })}>
                    <span className="material-icons">
                        keyboard_arrow_right
                    </span>
                </li>
            </ul>
        </PaginationSimpleWrapper>
    )
}
