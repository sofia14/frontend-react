import styled from 'styled-components'

import { colors } from '../../../assets/styles/colors'

export const ButtonWrapper = styled.button`
    cursor: pointer;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    border: none;
    padding: 1em 4em;
    color: ${colors.white};
    background-color: ${colors.primary};
    border-radius: 5px;
    text-decoration: none;
    height: 35px;
  
    &:disabled {
        pointer-events: none;
        opacity: 0.6;
    }
    
    &.button {
      &--primary {
        background-color: ${colors.primary};
      }
    
      &--secondary {
        background-color: ${colors.secondary};
      }
    }
    
    &--small {
        min-width: 100px;
        padding: 0.75em 1em;
    }
`
