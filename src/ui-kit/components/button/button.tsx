import React, { FunctionComponent } from 'react'
import classnames from 'classnames'
import { ButtonWrapper } from './button.styles'

export type ButtonType = 'button' | 'submit' | 'reset'

export type ButtonColor = 'primary' | 'secondary'

export interface ButtonProps {
    disabled?: boolean
    label: string
    handleClick?: () => void
    small?: boolean
    type?: ButtonType
    color?: ButtonColor
}

export const Button: FunctionComponent<ButtonProps> = (
    {
        small = false,
        disabled = false,
        type = 'button',
        handleClick,
        label = '',
        color = 'primary',
    }
) => {
    const onClick = () => {
        handleClick && handleClick()
    }

    return (
        <ButtonWrapper
            type={type}
            disabled={disabled}
            className={classnames('button', `button--${color}`, { 'button--small': small })}
            onClick={onClick}>
            {label}
        </ButtonWrapper>
    )
}
