import React from 'react'
import { act, renderIntoDocument, Simulate } from 'react-dom/test-utils'

import { findDOMNode, render, unmountComponentAtNode } from 'react-dom'
import { Button, ButtonColor } from './button'

describe('Button', () => {
    let container: Element

    beforeEach(() => {
        container = document.createElement('div')
        document.body.appendChild(container)
    })

    afterEach(() => {
        unmountComponentAtNode(container)
        container.remove()
    })

    test('has class button by default', () => {
        const button = <Button
            label="Submit"
        />

        act(() => {
            render(button, container)
        })

        expect(container.firstChild).toHaveClass('button')
    })

    test('small button is not by default', () => {
        const button = <Button
            label="Submit"
        />

        act(() => {
            render(button, container)
        })

        const { classList } = container.firstChild as HTMLButtonElement
        expect(classList.contains('small')).toBeFalsy()
    })

    test('test handle click', () => {
        const mockClick = jest.fn()
        const element = renderIntoDocument(
            <div>
                <Button label="Cancel" handleClick={mockClick}/>
            </div>
        ) as any

        const button = findDOMNode(element) as HTMLButtonElement
        Simulate.click(button)
    })

    test('colors', () => {
        const colors: ButtonColor[] = [
            'primary',
            'secondary',
        ]

        for (const color of colors) {
            const button = <Button
                label="Submit"
                color={color}
            />

            // eslint-disable-next-line no-loop-func
            act(() => {
                render(button, container)
            })

            const { classList } = container.firstChild as HTMLButtonElement
            expect(classList.contains(`button--${color}`)).toBeTruthy()
        }
    })
})
