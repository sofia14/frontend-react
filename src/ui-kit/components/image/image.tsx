import React, { Fragment, FunctionComponent, useState } from 'react'
import { Loader } from '../loader/loader'

export interface ImageProps {
    image: string
}

export const Image: FunctionComponent<ImageProps> = (
    {
        image,
    }
) => {

    const [status, setStatus] = useState('loading')

    const onLoad = () => {
        setStatus('loaded')
    }

    const onError = () => {
        setStatus('error')
    }

    return (
        <Fragment>
            {
                (status == 'loading') ?
                    <Loader variant="white"/> : null}
            <img
                src={image}
                onLoad={onLoad}
                onError={onError}
            />
        </Fragment>
    )
}
