import React, { ChangeEvent, FunctionComponent, useRef, useState } from 'react'
import { throttle } from 'lodash'

import { SearchWrapper } from './search.styles'
import { Loader } from '../loader/loader'


export interface SearchProps {
    placeholder?: string

    loading: boolean

    handleSearch(value: string): void
}

export const Search: FunctionComponent<SearchProps> = (
    {
        placeholder = 'Search...',
        loading,
        handleSearch
    }
) => {

    const [search, setSearch] = useState('')

    const throttleSearch = useRef(throttle(handleSearch, 500)).current

    const onSearch = (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value
        setSearch(value)
        throttleSearch(value)
    }

    const onReset = () => {
        setSearch('')
        handleSearch('')
    }

    return (
        <SearchWrapper>
            <div className='search_bar'>
                <input id='search' type='checkbox' checked={true}/>
                <label htmlFor='search'>
                    <span className="icon material-icons">
                        search
                    </span>
                    <span
                        onClick={onReset}
                        className="last icon material-icons"
                    >
                        {loading ? <Loader small={true}/> : 'close'}
                    </span>
                    <p>|</p>
                </label>
                <input
                    placeholder={placeholder}
                    type='text'
                    onChange={onSearch}
                    value={search}/>
            </div>
        </SearchWrapper>
    )
}
