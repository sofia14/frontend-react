import styled from 'styled-components'

import { colors } from '../../../assets/styles/colors'

export const SearchWrapper = styled.div`
    text-align:center;
    overflow:hidden;
    width: 100%;
    
    .search_bar {
      position:relative;
      input[type="text"]{
        width:15px;
        background:transparent;
        transition:border .3s 0s, width .2s .3s cubic-bezier(0.225, 0.010, 0.475, 1.010), text-indent .2s .3s;
        padding: 20px;
        text-indent:30px;
        outline:none;
        border:0 solid ${colors.primary};
        font-size: 12px;
        color: ${colors.secondary};
        border-radius:5px;

      }
      ::placeholder {
        color: ${colors.primary};
        font-weight:400;
      }

      input[type="checkbox"]{
        display:none;
      }
      input[type="checkbox"]:checked + label + input{
        width: 100%;
        border: 5px solid ${colors.primary};
        text-indent:0;
      }
      input[type="checkbox"]:checked + label span{
        right: 0;
        transform: translateY(-50%) translateX(50%) rotate(360deg) scale(0);
        color: ${colors.primary};
      }
      input[type="checkbox"]:checked + label .last{
        transform: translateY(-50%) rotate(360deg) scale(1);
     
      }
      input[type="checkbox"]:checked + label p{
        top: 50%;
        transition:all .3s .45s;
      }
      input[type="checkbox"]:not(checked) + label p{
        top: -50%;
        transition:all .3s 0s;
      }
      .last{
        -webkit-transform: translateY(-50%) rotate(0deg) scale(0);
        transform: translateY(-50%) rotate(0deg) scale(0);
      }
      span {
        position: absolute;
        font-size: 30px;
        top: 50%;
        transform: translateY(-50%) translateX(50%) rotate(0deg) scale(1);
        cursor: pointer;
        z-index: 2;
        margin: auto;
        border-radius: 4px;
        width: 56px;
        right: 50%;
        height: 54px;
        background: transparent;
        transition: right .3s .3s, transform .3s .3s, color .3s;
        line-height: 60px;
        color: ${colors.primary};
        //&:hover{
        //  color:#448996;
        //}
      }
      
      p{
        position: absolute;
        margin: 0;
        right: 52px;
        color: ${colors.primary};
        font-weight: 700;
        font-size: 30px;
        top: -50%;
        transform: translateY(-50%) rotate(0deg) scale(1);
      }
    }
`
