import styled from 'styled-components'

export const LayoutWrapper = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    justify-content: center;
    
    > div {
      display: flex;
      flex-direction: column;
      justify-content: center;
      padding: 2em;
      min-width: 400px;
    }
`
