import React, { FunctionComponent } from 'react'
import { Route, Switch } from 'react-router-dom'

import { NotFound } from '../../modules/core/views/not-found'
import { PrivateRoute } from '../../routing/private-route'
import { DashboardView } from '../../modules/dashboard/default/dashboard.view'
import { PlayerListView } from '../../modules/players/player-list/player-list.view'
import { Menu } from '../components/menu/menu'
import { PlayerDetailView } from '../../modules/players/player-detail/player-detail.view'
import { TournamentDetailView } from '../../modules/tournaments/tournament-detail/tournament-detail.view'
import { TicketListView } from '../../modules/tickets/views/ticket-list/ticket-list.view'
import { TournamentListContainer } from '../../modules/tournaments/tournament-list/wrappers/tournament-list.container'
import { DownloadView } from '../../modules/download/views/download.view'
import { LogsView } from '../../modules/Logs/views/logs.view'

export interface AuthLayoutProps {

}

export const AuthLayout: FunctionComponent<AuthLayoutProps> = () => {
    return (
        <div>
            <Menu/>
            <Switch>
                <PrivateRoute path="/dashboard" component={DashboardView}/>
                <PrivateRoute path="/players" component={PlayerListView}/>
                <PrivateRoute path="/player/:id" component={PlayerDetailView}/>
                <PrivateRoute path="/tournaments" component={TournamentListContainer}/>
                <PrivateRoute path="/tournament/:id" component={TournamentDetailView}/>
                <PrivateRoute path="/tickets" component={TicketListView}/>
                <PrivateRoute path="/download" component={DownloadView}/>
                <PrivateRoute path="/logs" component={LogsView}/>
                <Route path="*" component={NotFound}/>
            </Switch>
        </div>
    )
}
