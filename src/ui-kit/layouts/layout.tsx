import React, { FunctionComponent } from 'react'

import { LayoutWrapper } from './layout.styles'

export interface LayoutProps {

}

export const Layout: FunctionComponent<LayoutProps> = (
    {
        children,
    },
) => {
    return (
        <LayoutWrapper>
            <div className="wrapper">
                {children}
            </div>
        </LayoutWrapper>
    )
}
