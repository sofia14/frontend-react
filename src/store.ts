import { combineReducers, createStore, Store } from 'redux'

import { UIReducer, UIState } from './modules/core/store/ui/ui.reducer'

export interface IApplicationState {
    ui: UIState
}

const rootReducer = combineReducers<IApplicationState>({
    ui: UIReducer
})

export function configureStore(): Store<IApplicationState> {
    return createStore(
        rootReducer,
        undefined
    )
}
