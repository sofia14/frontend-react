import React, { FunctionComponent } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Store } from 'redux'

import * as serviceWorker from './serviceWorker'
import './assets/styles/main.scss'
import { Routes } from './routing/routes'
import { configureStore, IApplicationState } from './store'

interface IProps {
    store: Store<IApplicationState>;
}

const Root: FunctionComponent<IProps> = props => {
    return (
        <Provider store={props.store}>
            <Routes/>
        </Provider>
    )
}

const store = configureStore()

ReactDOM.render(
    <React.StrictMode>
        <Root store={store}>
            <Routes/>
        </Root>
    </React.StrictMode>,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
