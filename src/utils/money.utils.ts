export const formatMoney = (money: number, locale = 'en-Us', currency = 'USD') => {
    if (!money) {
        return ''
    }

    return new Intl.NumberFormat(locale, {
        style: 'currency',
        currency
    }).format(money)
}
