export interface Filters {
    [key: string]: string | number
}

export const cleanFilters = (filters: Filters) => {
    return Object.keys(filters).reduce((prev, key) => {
        if (filters[key]) {
            return { ...prev, [key]: filters[key] }
        }

        return prev
    }, {})
}

export const toQueryString = (filters: Filters, prefix = '?') => {
    const keys = Object.keys(filters)

    if (keys.length < 1) {
        return ''
    }

    return prefix + keys.map(key => key + '=' + filters[key]).join('&')
}
