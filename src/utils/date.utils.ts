export const getDate = (date: string) => {
    if (!date) {
        return ''
    }

    const obj = new Date(date)
    return `${obj.getDate()}.${obj.getUTCMonth() + 1}.${obj.getUTCFullYear()}`
}
