import React, { FunctionComponent } from 'react'
import { Redirect, Route } from 'react-router-dom'

import { CustomRouteProps } from './routes'
import { authApi } from '../modules/auth/auth.api'

export const PublicRoute: FunctionComponent<CustomRouteProps> = (
    {
        component: Component,
        path,
        ...rest
    }
) => {
    const authenticated = authApi.isLoggedIn()

    return (
        <Route {...rest} render={(props) => {
            props = { ...props, ...rest }
            return authenticated
                ? <Redirect to='/dashboard'/>
                : <Component {...props} />
        }}/>
    )
}

