import React, { ComponentType } from 'react'
import { BrowserRouter as Router, Redirect, Route, RouteComponentProps, RouteProps, Switch } from 'react-router-dom'

import { PublicRoute } from './public-route'
import { Register } from '../modules/auth/views/register'
import { ForgotPassword } from '../modules/auth/views/forgot-password'
import { NotFound } from '../modules/core/views/not-found'
import { LoginView } from '../modules/auth/login/login.view'
import { AuthLayout } from '../ui-kit/layouts/auth-layout'
import { PrivateRoute } from './private-route'

export interface CustomRouteProps extends RouteProps {
    component: ComponentType<RouteComponentProps<any>> | ComponentType<any>;
    path: string
    rest?: any
    id?: string
}

export const Routes = () => {
    return (
        <Router>
            <Switch>
                <Redirect exact from="/" to="/login"/>
                <PublicRoute path="/login" component={LoginView}/>
                <PublicRoute path="/register" component={Register}/>
                <PublicRoute path="/forgot-password" component={ForgotPassword}/>
                <PrivateRoute path="/" component={AuthLayout}/>
                <Route path="*" component={NotFound}/>
            </Switch>
        </Router>
    )
}
